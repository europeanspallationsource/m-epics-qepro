#!/bin/sh
#
# The purpose of this script is to check to see if installation of the
# OmniDriver+SPAM SDK appears to have been successful.
#
# Please note that this will only check environment settings, not project
# settings.  You will have to configure your compiler and linker options for
# your specific IDE.

# Check to see if the symbolic links exist
if [ ! -f /usr/lib/libcommon.so ] ; then
	echo FAIL: Symbolic link /usr/lib/libcommon.so does not exist...
	echo FAIL: The installer should have done this for you...
	echo FAIL: You can make this link by issuing the following command as root:
	echo FAIL:     ln -s /path/to/installation/OOI_HOME/libcommon.so /usr/lib/libcommon.so
	echo FAIL: where /path/to/installation is the path of your OmniDriver+SPAM installation
	echo
else
	echo PASS: Symbolic link /usr/lib/libcommon.so exists...
	echo
fi

if [ ! -f /usr/lib/libOmniDriver.so ] ; then
	echo FAIL: Symbolic link /usr/lib/libOmniDriver.so does not exist...
	echo FAIL: The installer should have done this for you...
	echo FAIL: You can make this link by issuing the following command as root:
	echo FAIL:     ln -s /path/to/installation/OOI_HOME/libOmniDriver.so /usr/lib/libOmniDriver.so
	echo FAIL: where /path/to/installation is the path of your OmniDriver+SPAM installation
	echo
else
	echo PASS: Symbolic link /usr/lib/libOmniDriver.so exists...
	echo
fi

if [ ! -f /usr/lib/libSPAM.so ] ; then
	echo FAIL: Symbolic link /usr/lib/libSPAM.so does not exist...
	echo FAIL: The installer should have done this for you...
	echo FAIL: You can make this link by issuing the following command as root:
	echo FAIL:     ln -s /path/to/installation/OOI_HOME/libSPAM.so /usr/lib/libSPAM.so
	echo FAIL: where /path/to/installation is the path of your OmniDriver+SPAM installation
	echo
else
	echo PASS: Symbolic link /usr/lib/libSPAM.so exists...
	echo
fi

if [ ! -f /usr/lib/libjvm.so ] ; then
	echo FAIL: Symbolic link /usr/lib/libjvm.so does not exist...
	echo FAIL: You can make this link by issuing the following command as root:
	echo FAIL:     ln -s /path/to/jdk/jre/lib/386/client/libjvm.so /usr/lib/libjvm.so
	echo FAIL: where /path/to/jdk is the path of your JDK installation
	echo
else
	echo PASS: Symbolic link /usr/lib/libjvm.so exists...
	echo
fi

# Check to see the value of the OOI_HOME environment variable
if [ `echo ${OOI_HOME} | wc -c` == "1" ] ; then
	echo FAIL: OOI_HOME Environment variable is empty
	echo FAIL: The installer should have done this for you...
	echo FAIL: You can fix this by editing the /etc/profile file as root and adding the following line:
	echo FAIL:     export OOI_HOME=/path/to/installation/OOI_HOME
	echo FAIL: where /path/to/installation is the path of your OmniDriver+SPAM installation
	echo
else
	echo PASS: Value of OOI_HOME Variable is set
	echo
fi

# Check to see if the value of the JAVA_HOME environment variable is set
if [ `echo ${JAVA_HOME} | wc -c` == "1" ] ; then
	echo FAIL: JAVA_HOME Environment variable is empty
	echo FAIL: You need to point this environment variable to the location of your JDK.
	echo FAIL: You can fix this by editing the /etc/profile file as root and adding the following line:
	echo FAIL:     export JAVA_HOME=/path/to/jdk
	echo FAIL: where /path/to/jdk is the path of your JDK installation
else
	echo PASS: Value of JAVA_HOME Variable is set
	echo
fi
