/**
 * File: Spectrum.h
 * Autogenerated on Mon Apr 11 10:06:15 EDT 2016 based on
 * Spectrum.java
 * for the Java class
 * com/oceanoptics/omnidriver/spectra/Spectrum
 * Copyright (C) 2006 - 2008 Ocean Optics, Inc.  All rights reserved.
 */


#ifndef SPECTRUM_H
#define SPECTRUM_H
#include "BaseJavaClass.h"

#ifdef __cplusplus

#ifdef INCLUDES_IN_HEADER
#endif /* INCLUDES_IN_HEADER */

/* Pre-declarations for circular dependencies in header files */
#ifdef CLASS_PREDECLARATIONS
class Spectrum;
#endif /* CLASS_PREDECLARATIONS */

CLASS_TOKEN EXPORTED Spectrum : public BaseJavaClass // CPPClass.tag001
{
public: 
	Spectrum(int totalpixels, int darkPixels);
	Spectrum(DoubleArray& spectrum, DoubleArray& darkPixels);
	DoubleArray getSpectrum();
	void setSpectrum(DoubleArray& spectrum);
	short isOfSize(int numberOfPixels, int numberOfDarkPixels);
	int getNumberOfDarkPixels();
	DoubleArray getDarkPixels();
	short isSameSizeAs(Spectrum& other);
	short isSaturated();
	void setSaturated(short saturated);
	~Spectrum();
	// No public default Java constructor; creating one:
	Spectrum();
	// No public Java copy constructor; creating one:
	Spectrum(const Spectrum &that);
	// Creating assignment operator declaration:
	Spectrum &operator=(const Spectrum &that);

private:
	jmethodID mid_Spectrum2395;
	jmethodID mid_Spectrum2396;
	jmethodID mid_getSpectrum2397;
	jmethodID mid_setSpectrum2398;
	jmethodID mid_isOfSize2399;
	jmethodID mid_getNumberOfDarkPixels2400;
	jmethodID mid_getDarkPixels2401;
	jmethodID mid_isSameSizeAs2402;
	jmethodID mid_isSaturated2403;
	jmethodID mid_setSaturated2404;
	void init_ids(JNIEnv* pJNIEnv);
};

extern "C" {
#endif /* __cplusplus */
#ifndef EXTERN_TYPEDEF_SPECTRUM_T
#define EXTERN_TYPEDEF_SPECTRUM_T
	typedef void* SPECTRUM_T;
#endif /* EXTERN_TYPEDEF_SPECTRUM_T */

	// No public default Java constructor; creating one:
	EXPORTED SPECTRUM_T Spectrum_Create();
	EXPORTED SPECTRUM_T Spectrum_Create_1(int totalpixels, int darkPixels);
	EXPORTED SPECTRUM_T Spectrum_Create_2(DOUBLEARRAY_T spectrum, DOUBLEARRAY_T darkPixels);
	EXPORTED void Spectrum_getSpectrum(SPECTRUM_T c_spectrum, DOUBLEARRAY_T retval);
	EXPORTED void Spectrum_setSpectrum(SPECTRUM_T c_spectrum, DOUBLEARRAY_T spectrum);
	EXPORTED short Spectrum_isOfSize(SPECTRUM_T c_spectrum, int numberOfPixels, int numberOfDarkPixels);
	EXPORTED int Spectrum_getNumberOfDarkPixels(SPECTRUM_T c_spectrum);
	EXPORTED void Spectrum_getDarkPixels(SPECTRUM_T c_spectrum, DOUBLEARRAY_T retval);
	EXPORTED short Spectrum_isSameSizeAs(SPECTRUM_T c_spectrum, SPECTRUM_T other);
	EXPORTED short Spectrum_isSaturated(SPECTRUM_T c_spectrum);
	EXPORTED void Spectrum_setSaturated(SPECTRUM_T c_spectrum, short saturated);
	EXPORTED void Spectrum_Destroy(SPECTRUM_T spectrum);

#ifdef WIN32
	// Use the following function prototypes when calling functions from Visual Basic 6
	EXPORTED SPECTRUM_T STDCALL Spectrum_Create_stdcall_1(int totalpixels, int darkPixels);
	EXPORTED SPECTRUM_T STDCALL Spectrum_Create_stdcall_2(DOUBLEARRAY_T spectrum, DOUBLEARRAY_T darkPixels);
	EXPORTED void STDCALL Spectrum_getSpectrum_stdcall(SPECTRUM_T c_spectrum, DOUBLEARRAY_T retval);
	EXPORTED void STDCALL Spectrum_setSpectrum_stdcall(SPECTRUM_T c_spectrum, DOUBLEARRAY_T spectrum);
	EXPORTED short STDCALL Spectrum_isOfSize_stdcall(SPECTRUM_T c_spectrum, int numberOfPixels, int numberOfDarkPixels);
	EXPORTED int STDCALL Spectrum_getNumberOfDarkPixels_stdcall(SPECTRUM_T c_spectrum);
	EXPORTED void STDCALL Spectrum_getDarkPixels_stdcall(SPECTRUM_T c_spectrum, DOUBLEARRAY_T retval);
	EXPORTED short STDCALL Spectrum_isSameSizeAs_stdcall(SPECTRUM_T c_spectrum, SPECTRUM_T other);
	EXPORTED short STDCALL Spectrum_isSaturated_stdcall(SPECTRUM_T c_spectrum);
	EXPORTED void STDCALL Spectrum_setSaturated_stdcall(SPECTRUM_T c_spectrum, short saturated);
	EXPORTED void STDCALL Spectrum_Destroy_stdcall(SPECTRUM_T spectrum);
#endif /* WIN32 */
#ifdef __cplusplus
};
#endif /* __cplusplus */
#endif /* SPECTRUM_H */
