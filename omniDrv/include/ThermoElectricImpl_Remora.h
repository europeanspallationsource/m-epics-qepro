/**
 * File: ThermoElectricImpl_Remora.h
 * Autogenerated on Mon Apr 11 10:06:12 EDT 2016 based on
 * ThermoElectricImpl_Remora.java
 * for the Java class
 * com/oceanoptics/omnidriver/features/thermoelectric/ThermoElectricImpl_Remora
 * Copyright (C) 2006 - 2008 Ocean Optics, Inc.  All rights reserved.
 */


#ifndef THERMOELECTRICIMPL_REMORA_H
#define THERMOELECTRICIMPL_REMORA_H
#include "BaseJavaClass.h"

#ifdef __cplusplus

#ifdef INCLUDES_IN_HEADER
#include "ThermoElectricImpl.h"
#include "JStringArray.h"
#endif /* INCLUDES_IN_HEADER */

/* Pre-declarations for circular dependencies in header files */
#ifdef CLASS_PREDECLARATIONS
class ThermoElectricImpl;
class JStringArray;
#endif /* CLASS_PREDECLARATIONS */

CLASS_TOKEN EXPORTED ThermoElectricImpl_Remora : public ThermoElectricImpl // CPPClass.tag001
{
public: 
	void setTECEnable(short enable);
	void setFanEnable(short enable);
	double getDetectorTemperatureCelsius();
	double getDetectorTemperatureSetPointCelsius();
	void setDetectorSetPointCelsius(double tempCelsius);
	double getSetPointMinimumCelsius();
	double getSetPointMaximumCelsius();
	double getSetPointIncrementCelsius();
	short isSaveTECStateEnabled();
	void saveTECState();
	JStringArray getFeatureGUIClassnames();
	~ThermoElectricImpl_Remora();
	// No public default Java constructor; creating one:
	ThermoElectricImpl_Remora();
	// No public Java copy constructor; creating one:
	ThermoElectricImpl_Remora(const ThermoElectricImpl_Remora &that);
	// Creating assignment operator declaration:
	ThermoElectricImpl_Remora &operator=(const ThermoElectricImpl_Remora &that);

private:
	jmethodID mid_setTECEnable1879;
	jmethodID mid_setFanEnable1880;
	jmethodID mid_getDetectorTemperatureCelsius1881;
	jmethodID mid_getDetectorTemperatureSetPointCelsius1882;
	jmethodID mid_setDetectorSetPointCelsius1883;
	jmethodID mid_getSetPointMinimumCelsius1884;
	jmethodID mid_getSetPointMaximumCelsius1885;
	jmethodID mid_getSetPointIncrementCelsius1886;
	jmethodID mid_isSaveTECStateEnabled1887;
	jmethodID mid_saveTECState1888;
	jmethodID mid_getFeatureGUIClassnames1889;
	void init_ids(JNIEnv* pJNIEnv);
};

extern "C" {
#endif /* __cplusplus */
#ifndef EXTERN_TYPEDEF_THERMOELECTRICIMPL_T
#define EXTERN_TYPEDEF_THERMOELECTRICIMPL_T
	typedef void* THERMOELECTRICIMPL_T;
#endif /* EXTERN_TYPEDEF_THERMOELECTRICIMPL_T */
#ifndef EXTERN_TYPEDEF_JSTRINGARRAY_T
#define EXTERN_TYPEDEF_JSTRINGARRAY_T
	typedef void* JSTRINGARRAY_T;
#endif /* EXTERN_TYPEDEF_JSTRINGARRAY_T */
#ifndef EXTERN_TYPEDEF_THERMOELECTRICIMPL_REMORA_T
#define EXTERN_TYPEDEF_THERMOELECTRICIMPL_REMORA_T
	typedef void* THERMOELECTRICIMPL_REMORA_T;
#endif /* EXTERN_TYPEDEF_THERMOELECTRICIMPL_REMORA_T */

	// No public default Java constructor; creating one:
	EXPORTED THERMOELECTRICIMPL_REMORA_T ThermoElectricImpl_Remora_Create();
	EXPORTED void ThermoElectricImpl_Remora_setTECEnable(THERMOELECTRICIMPL_REMORA_T c_thermo_electric_impl_remora, short enable);
	EXPORTED void ThermoElectricImpl_Remora_setFanEnable(THERMOELECTRICIMPL_REMORA_T c_thermo_electric_impl_remora, short enable);
	EXPORTED double ThermoElectricImpl_Remora_getDetectorTemperatureCelsius(THERMOELECTRICIMPL_REMORA_T c_thermo_electric_impl_remora);
	EXPORTED double ThermoElectricImpl_Remora_getDetectorTemperatureSetPointCelsius(THERMOELECTRICIMPL_REMORA_T c_thermo_electric_impl_remora);
	EXPORTED void ThermoElectricImpl_Remora_setDetectorSetPointCelsius(THERMOELECTRICIMPL_REMORA_T c_thermo_electric_impl_remora, double tempCelsius);
	EXPORTED double ThermoElectricImpl_Remora_getSetPointMinimumCelsius(THERMOELECTRICIMPL_REMORA_T c_thermo_electric_impl_remora);
	EXPORTED double ThermoElectricImpl_Remora_getSetPointMaximumCelsius(THERMOELECTRICIMPL_REMORA_T c_thermo_electric_impl_remora);
	EXPORTED double ThermoElectricImpl_Remora_getSetPointIncrementCelsius(THERMOELECTRICIMPL_REMORA_T c_thermo_electric_impl_remora);
	EXPORTED short ThermoElectricImpl_Remora_isSaveTECStateEnabled(THERMOELECTRICIMPL_REMORA_T c_thermo_electric_impl_remora);
	EXPORTED void ThermoElectricImpl_Remora_saveTECState(THERMOELECTRICIMPL_REMORA_T c_thermo_electric_impl_remora);
	EXPORTED void ThermoElectricImpl_Remora_getFeatureGUIClassnames(THERMOELECTRICIMPL_REMORA_T c_thermo_electric_impl_remora, JSTRINGARRAY_T retval);
	EXPORTED void ThermoElectricImpl_Remora_Destroy(THERMOELECTRICIMPL_REMORA_T thermo_electric_impl_remora);

#ifdef WIN32
	// Use the following function prototypes when calling functions from Visual Basic 6
	EXPORTED void STDCALL ThermoElectricImpl_Remora_setTECEnable_stdcall(THERMOELECTRICIMPL_REMORA_T c_thermo_electric_impl_remora, short enable);
	EXPORTED void STDCALL ThermoElectricImpl_Remora_setFanEnable_stdcall(THERMOELECTRICIMPL_REMORA_T c_thermo_electric_impl_remora, short enable);
	EXPORTED double STDCALL ThermoElectricImpl_Remora_getDetectorTemperatureCelsius_stdcall(THERMOELECTRICIMPL_REMORA_T c_thermo_electric_impl_remora);
	EXPORTED double STDCALL ThermoElectricImpl_Remora_getDetectorTemperatureSetPointCelsius_stdcall(THERMOELECTRICIMPL_REMORA_T c_thermo_electric_impl_remora);
	EXPORTED void STDCALL ThermoElectricImpl_Remora_setDetectorSetPointCelsius_stdcall(THERMOELECTRICIMPL_REMORA_T c_thermo_electric_impl_remora, double tempCelsius);
	EXPORTED double STDCALL ThermoElectricImpl_Remora_getSetPointMinimumCelsius_stdcall(THERMOELECTRICIMPL_REMORA_T c_thermo_electric_impl_remora);
	EXPORTED double STDCALL ThermoElectricImpl_Remora_getSetPointMaximumCelsius_stdcall(THERMOELECTRICIMPL_REMORA_T c_thermo_electric_impl_remora);
	EXPORTED double STDCALL ThermoElectricImpl_Remora_getSetPointIncrementCelsius_stdcall(THERMOELECTRICIMPL_REMORA_T c_thermo_electric_impl_remora);
	EXPORTED short STDCALL ThermoElectricImpl_Remora_isSaveTECStateEnabled_stdcall(THERMOELECTRICIMPL_REMORA_T c_thermo_electric_impl_remora);
	EXPORTED void STDCALL ThermoElectricImpl_Remora_saveTECState_stdcall(THERMOELECTRICIMPL_REMORA_T c_thermo_electric_impl_remora);
	EXPORTED void STDCALL ThermoElectricImpl_Remora_getFeatureGUIClassnames_stdcall(THERMOELECTRICIMPL_REMORA_T c_thermo_electric_impl_remora, JSTRINGARRAY_T retval);
	EXPORTED void STDCALL ThermoElectricImpl_Remora_Destroy_stdcall(THERMOELECTRICIMPL_REMORA_T thermo_electric_impl_remora);
#endif /* WIN32 */
#ifdef __cplusplus
};
#endif /* __cplusplus */
#endif /* THERMOELECTRICIMPL_REMORA_H */
