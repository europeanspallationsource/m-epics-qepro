/**
 * File: HR4000.h
 * Autogenerated on Mon Apr 11 10:06:23 EDT 2016 based on
 * HR4000.java
 * for the Java class
 * com/oceanoptics/omnidriver/spectrometer/hr4000/HR4000
 * Copyright (C) 2006 - 2008 Ocean Optics, Inc.  All rights reserved.
 */


#ifndef HR4000_H
#define HR4000_H
#include "BaseJavaClass.h"

#ifdef __cplusplus

#ifdef INCLUDES_IN_HEADER
#include "USBSpectrometer.h"
#include "USBEndpointDescriptor.h"
#include "USBFeature.h"
#include "GUIProviderArray.h"
#include "OmniDriverDispatchListener.h"
#include "JString.h"
#include "BitSet.h"
#include "ExternalTriggerModeArray.h"
#include "CoefficientsArray.h"
#include "SpectrometerPlugInArray.h"
#include "SHAChannel.h"
#include "SpectrometerStatus.h"
#include "AcquisitionListener.h"
#endif /* INCLUDES_IN_HEADER */

/* Pre-declarations for circular dependencies in header files */
#ifdef CLASS_PREDECLARATIONS
class USBSpectrometer;
class USBEndpointDescriptor;
class USBFeature;
class GUIProviderArray;
class OmniDriverDispatchListener;
class JString;
class BitSet;
class ExternalTriggerModeArray;
class CoefficientsArray;
class SpectrometerPlugInArray;
class SHAChannel;
class SpectrometerStatus;
class AcquisitionListener;
#endif /* CLASS_PREDECLARATIONS */

CLASS_TOKEN EXPORTED HR4000 : public USBSpectrometer // CPPClass.tag001
{
public: 
	HR4000();
	HR4000(int i);
	void setEndpoints();
	USBEndpointDescriptor getEndpoint(int endPoint);
	short allowWriteToEEPROM(int privilegeLevel, int slot);
	void openSpectrometer(int index);
	USBFeature getFeatureControllerGPIO();
	GUIProviderArray getGUIFeatures();
	void readSpectrum(CharArray& data);
	void readSpectrum();
	int readIntegrationTime();
	void setIntegrationTime(int intTime);
	int getGatingModeIntegrationTime();
	void addOmniDriverDispatchListener(OmniDriverDispatchListener& listener);
	void removeOmniDriverDispatchListener(OmniDriverDispatchListener& listener);
	JString getName();
	short isSHAEnabled();
	void enableSHA(short enable);
	void setStrobeDelay(int delay);
	JString toString();
	void setAdvancedIntegrationTime(long long delayMicros);
	int getIntegrationTimeBaseClock();
	int getIntegrationClockTimer();
	long long getAdvancedIntegrationTimeMinimum();
	long long getAdvancedIntegrationTimeMaximum();
	long long getAdvancedIntegrationTimeIncrement();
	JString getPSOCVersion();
	JString getFPGAFirmwareVersion();
	double getVoltageIn();
	void setDACCounts(int counts, int index);
	double analogOutCountsToVolts(int counts);
	int getDACMinimum();
	int getDACMaximum();
	int getDACIncrement();
	short isDACPresent();
	int getDACPins();
	double getBoardTemperatureCelsius();
	void setContinuousStrobeDelay(int delayMicros);
	int getContinuousStrobeDelayMinimum();
	int getContinuousStrobeDelayMaximum();
	int getContinuousStrobeDelayIncrement(int magnitude);
	double continuousStrobeCountsToMicros(int counts);
	void setContinuousModeType(short mode);
	void setDelayAfterIntegration(int delay);
	short getContinuousModeType();
	int getDelayAfterIntegration();
	void setContinuousEnable(short value);
	short getContinuousEnable();
	void setExternalTriggerDelay(int counts);
	double triggerDelayCountsToMicroseconds(int counts);
	int getExternalTriggerDelayMinimum();
	int getExternalTriggerDelayMaximum();
	int getExternalTriggerDelayIncrement();
	void setDirectionAllBits(BitSet& bitSet);
	void setMuxAllBits(BitSet& bitSet);
	void setValueAllBits(BitSet& bitSet);
	void setDirectionBitmask(short bitmask);
	void setMuxBitmask(short bitmask);
	void setValueBitmask(short bitmask);
	void setDirectionBit(int bit, short value);
	void setMuxBit(int bit, short value);
	void setValueBit(int bit, short value);
	int getTotalGPIOBits();
	BitSet getDirectionBits();
	BitSet getMuxBits();
	int getValueBit(int bitNumber);
	BitSet getValueBits();
	int getNumberOfPins();
	void setExternalTriggerMode(int mode);
	ExternalTriggerModeArray getExternalTriggerModes();
	int setI2CBytes(signed char address, signed char numBytes, CharArray& i2C);
	CharArray getI2CBytes(signed char address, signed char numBytes);
	int setSHAI2CBytes(signed char address, signed char numBytes, CharArray& i2C);
	CharArray getSHAI2CBytes(signed char address, signed char numBytes);
	DoubleArray getIrradianceCalibrationFactors();
	void setIrradianceCalibrationFactors(DoubleArray& data);
	double getCollectionArea();
	short hasCollectionArea();
	void setCollectionArea(double area);
	void setMasterClockDivisor(int value);
	int getMasterClockDivisor();
	CoefficientsArray readNonlinearityCoefficientsFromSpectrometer();
	void writeNonlinearityCoefficientsToSpectrometer(CoefficientsArray& coefficients);
	CoefficientsArray getNonlinearityCoefficients();
	void setNonlinearityCoefficients(CoefficientsArray& coefficients);
	DoubleArray getNonlinearityCoefficientsSingleChannel(int index);
	void setNonlinearityCoefficientsSingleChannel(DoubleArray& nl, int index);
	SpectrometerPlugInArray getPlugIns();
	int getNumberOfPlugIns();
	short isPlugInDetected(int id);
	CharArray initializePlugIns();
	void detectPlugIns();
	short isHyperAdapterPresent();
	void initSHA(SHAChannel& channel);
	int setHyperAdapterDAC(double voltage);
	int readHyperAdapterEEPROM(CharArray& data, int start, int length);
	int writeHyperAdapterEEPROM(CharArray& data, int start, int length);
	void calibrate();
	double getCalibrationTime();
	void restoreCalibration();
	SHAChannel getSHAChannel();
	void setShutterClock(int value);
	int getShutterClock();
	void setSingleStrobeLow(int value);
	void setSingleStrobeHigh(int value);
	double getSingleStrobeCountsToMicros(int counts);
	int getSingleStrobeLow();
	int getSingleStrobeHigh();
	int getSingleStrobeMinimum();
	int getSingleStrobeMaximum();
	int getSingleStrobeIncrement();
	CharArray getSPIBytes(CharArray& message, int length);
	SpectrometerStatus getStatus();
	CoefficientsArray readStrayLightCorrectionCoefficientFromSpectrometer();
	void writeStrayLightCoefficientToSpectrometer(CoefficientsArray& coefficients);
	void setStrayLightCorrectionCoefficient(CoefficientsArray& coefficients);
	CoefficientsArray getStrayLightCorrectionCoefficient();
	void setStrayLight(double strayLight, int index);
	double getStrayLight(int index);
	CoefficientsArray readWavelengthCalibrationCoefficientsFromSpectrometer();
	void writeWavelengthCoefficientsToSpectrometer(CoefficientsArray& coefficients);
	CoefficientsArray getWavelengthCalibrationCoefficients();
	void setWavelengthCalibrationCoefficients(CoefficientsArray& coefficients);
	DoubleArray getWavelengths(int index);
	void setWavelengths(DoubleArray& wl, int index);
	short isAdvancedVersion();
	void addAcquisitionListener(AcquisitionListener& listener);
	void removeAcquisitionListener(AcquisitionListener& listener);
	~HR4000();
	// No public Java copy constructor; creating one:
	HR4000(const HR4000 &that);
	// Creating assignment operator declaration:
	HR4000 &operator=(const HR4000 &that);

private:
	jmethodID mid_HR40003214;
	jmethodID mid_HR40003215;
	jmethodID mid_setEndpoints3216;
	jmethodID mid_getEndpoint3217;
	jmethodID mid_allowWriteToEEPROM3218;
	jmethodID mid_openSpectrometer3219;
	jmethodID mid_getFeatureControllerGPIO3220;
	jmethodID mid_getGUIFeatures3221;
	jmethodID mid_readSpectrum3222;
	jmethodID mid_readSpectrum3223;
	jmethodID mid_readIntegrationTime3224;
	jmethodID mid_setIntegrationTime3225;
	jmethodID mid_getGatingModeIntegrationTime3226;
	jmethodID mid_addOmniDriverDispatchListener3227;
	jmethodID mid_removeOmniDriverDispatchListener3228;
	jmethodID mid_getName3229;
	jmethodID mid_isSHAEnabled3230;
	jmethodID mid_enableSHA3231;
	jmethodID mid_setStrobeDelay3232;
	jmethodID mid_toString3233;
	jmethodID mid_setAdvancedIntegrationTime3234;
	jmethodID mid_getIntegrationTimeBaseClock3235;
	jmethodID mid_getIntegrationClockTimer3236;
	jmethodID mid_getAdvancedIntegrationTimeMinimum3237;
	jmethodID mid_getAdvancedIntegrationTimeMaximum3238;
	jmethodID mid_getAdvancedIntegrationTimeIncrement3239;
	jmethodID mid_getPSOCVersion3240;
	jmethodID mid_getFPGAFirmwareVersion3241;
	jmethodID mid_getVoltageIn3242;
	jmethodID mid_setDACCounts3243;
	jmethodID mid_analogOutCountsToVolts3244;
	jmethodID mid_getDACMinimum3245;
	jmethodID mid_getDACMaximum3246;
	jmethodID mid_getDACIncrement3247;
	jmethodID mid_isDACPresent3248;
	jmethodID mid_getDACPins3249;
	jmethodID mid_getBoardTemperatureCelsius3250;
	jmethodID mid_setContinuousStrobeDelay3251;
	jmethodID mid_getContinuousStrobeDelayMinimum3252;
	jmethodID mid_getContinuousStrobeDelayMaximum3253;
	jmethodID mid_getContinuousStrobeDelayIncrement3254;
	jmethodID mid_continuousStrobeCountsToMicros3255;
	jmethodID mid_setContinuousModeType3256;
	jmethodID mid_setDelayAfterIntegration3257;
	jmethodID mid_getContinuousModeType3258;
	jmethodID mid_getDelayAfterIntegration3259;
	jmethodID mid_setContinuousEnable3260;
	jmethodID mid_getContinuousEnable3261;
	jmethodID mid_setExternalTriggerDelay3262;
	jmethodID mid_triggerDelayCountsToMicroseconds3263;
	jmethodID mid_getExternalTriggerDelayMinimum3264;
	jmethodID mid_getExternalTriggerDelayMaximum3265;
	jmethodID mid_getExternalTriggerDelayIncrement3266;
	jmethodID mid_setDirectionAllBits3267;
	jmethodID mid_setMuxAllBits3268;
	jmethodID mid_setValueAllBits3269;
	jmethodID mid_setDirectionBitmask3270;
	jmethodID mid_setMuxBitmask3271;
	jmethodID mid_setValueBitmask3272;
	jmethodID mid_setDirectionBit3273;
	jmethodID mid_setMuxBit3274;
	jmethodID mid_setValueBit3275;
	jmethodID mid_getTotalGPIOBits3276;
	jmethodID mid_getDirectionBits3277;
	jmethodID mid_getMuxBits3278;
	jmethodID mid_getValueBit3279;
	jmethodID mid_getValueBits3280;
	jmethodID mid_getNumberOfPins3281;
	jmethodID mid_setExternalTriggerMode3282;
	jmethodID mid_getExternalTriggerModes3283;
	jmethodID mid_setI2CBytes3284;
	jmethodID mid_getI2CBytes3285;
	jmethodID mid_setSHAI2CBytes3286;
	jmethodID mid_getSHAI2CBytes3287;
	jmethodID mid_getIrradianceCalibrationFactors3288;
	jmethodID mid_setIrradianceCalibrationFactors3289;
	jmethodID mid_getCollectionArea3290;
	jmethodID mid_hasCollectionArea3291;
	jmethodID mid_setCollectionArea3292;
	jmethodID mid_setMasterClockDivisor3293;
	jmethodID mid_getMasterClockDivisor3294;
	jmethodID mid_readNonlinearityCoefficientsFromSpectrometer3295;
	jmethodID mid_writeNonlinearityCoefficientsToSpectrometer3296;
	jmethodID mid_getNonlinearityCoefficients3297;
	jmethodID mid_setNonlinearityCoefficients3298;
	jmethodID mid_getNonlinearityCoefficientsSingleChannel3299;
	jmethodID mid_setNonlinearityCoefficientsSingleChannel3300;
	jmethodID mid_getPlugIns3301;
	jmethodID mid_getNumberOfPlugIns3302;
	jmethodID mid_isPlugInDetected3303;
	jmethodID mid_initializePlugIns3304;
	jmethodID mid_detectPlugIns3305;
	jmethodID mid_isHyperAdapterPresent3306;
	jmethodID mid_initSHA3307;
	jmethodID mid_setHyperAdapterDAC3308;
	jmethodID mid_readHyperAdapterEEPROM3309;
	jmethodID mid_writeHyperAdapterEEPROM3310;
	jmethodID mid_calibrate3311;
	jmethodID mid_getCalibrationTime3312;
	jmethodID mid_restoreCalibration3313;
	jmethodID mid_getSHAChannel3314;
	jmethodID mid_setShutterClock3315;
	jmethodID mid_getShutterClock3316;
	jmethodID mid_setSingleStrobeLow3317;
	jmethodID mid_setSingleStrobeHigh3318;
	jmethodID mid_getSingleStrobeCountsToMicros3319;
	jmethodID mid_getSingleStrobeLow3320;
	jmethodID mid_getSingleStrobeHigh3321;
	jmethodID mid_getSingleStrobeMinimum3322;
	jmethodID mid_getSingleStrobeMaximum3323;
	jmethodID mid_getSingleStrobeIncrement3324;
	jmethodID mid_getSPIBytes3325;
	jmethodID mid_getStatus3326;
	jmethodID mid_readStrayLightCorrectionCoefficientFromSpectrometer3327;
	jmethodID mid_writeStrayLightCoefficientToSpectrometer3328;
	jmethodID mid_setStrayLightCorrectionCoefficient3329;
	jmethodID mid_getStrayLightCorrectionCoefficient3330;
	jmethodID mid_setStrayLight3331;
	jmethodID mid_getStrayLight3332;
	jmethodID mid_readWavelengthCalibrationCoefficientsFromSpectrometer3333;
	jmethodID mid_writeWavelengthCoefficientsToSpectrometer3334;
	jmethodID mid_getWavelengthCalibrationCoefficients3335;
	jmethodID mid_setWavelengthCalibrationCoefficients3336;
	jmethodID mid_getWavelengths3337;
	jmethodID mid_setWavelengths3338;
	jmethodID mid_isAdvancedVersion3339;
	jmethodID mid_addAcquisitionListener3340;
	jmethodID mid_removeAcquisitionListener3341;
	void init_ids(JNIEnv* pJNIEnv);
};

extern "C" {
#endif /* __cplusplus */
#ifndef EXTERN_TYPEDEF_USBSPECTROMETER_T
#define EXTERN_TYPEDEF_USBSPECTROMETER_T
	typedef void* USBSPECTROMETER_T;
#endif /* EXTERN_TYPEDEF_USBSPECTROMETER_T */
#ifndef EXTERN_TYPEDEF_USBENDPOINTDESCRIPTOR_T
#define EXTERN_TYPEDEF_USBENDPOINTDESCRIPTOR_T
	typedef void* USBENDPOINTDESCRIPTOR_T;
#endif /* EXTERN_TYPEDEF_USBENDPOINTDESCRIPTOR_T */
#ifndef EXTERN_TYPEDEF_USBFEATURE_T
#define EXTERN_TYPEDEF_USBFEATURE_T
	typedef void* USBFEATURE_T;
#endif /* EXTERN_TYPEDEF_USBFEATURE_T */
#ifndef EXTERN_TYPEDEF_GUIPROVIDERARRAY_T
#define EXTERN_TYPEDEF_GUIPROVIDERARRAY_T
	typedef void* GUIPROVIDERARRAY_T;
#endif /* EXTERN_TYPEDEF_GUIPROVIDERARRAY_T */
#ifndef EXTERN_TYPEDEF_OMNIDRIVERDISPATCHLISTENER_T
#define EXTERN_TYPEDEF_OMNIDRIVERDISPATCHLISTENER_T
	typedef void* OMNIDRIVERDISPATCHLISTENER_T;
#endif /* EXTERN_TYPEDEF_OMNIDRIVERDISPATCHLISTENER_T */
#ifndef EXTERN_TYPEDEF_JSTRING_T
#define EXTERN_TYPEDEF_JSTRING_T
	typedef void* JSTRING_T;
#endif /* EXTERN_TYPEDEF_JSTRING_T */
#ifndef EXTERN_TYPEDEF_BITSET_T
#define EXTERN_TYPEDEF_BITSET_T
	typedef void* BITSET_T;
#endif /* EXTERN_TYPEDEF_BITSET_T */
#ifndef EXTERN_TYPEDEF_EXTERNALTRIGGERMODEARRAY_T
#define EXTERN_TYPEDEF_EXTERNALTRIGGERMODEARRAY_T
	typedef void* EXTERNALTRIGGERMODEARRAY_T;
#endif /* EXTERN_TYPEDEF_EXTERNALTRIGGERMODEARRAY_T */
#ifndef EXTERN_TYPEDEF_COEFFICIENTSARRAY_T
#define EXTERN_TYPEDEF_COEFFICIENTSARRAY_T
	typedef void* COEFFICIENTSARRAY_T;
#endif /* EXTERN_TYPEDEF_COEFFICIENTSARRAY_T */
#ifndef EXTERN_TYPEDEF_SPECTROMETERPLUGINARRAY_T
#define EXTERN_TYPEDEF_SPECTROMETERPLUGINARRAY_T
	typedef void* SPECTROMETERPLUGINARRAY_T;
#endif /* EXTERN_TYPEDEF_SPECTROMETERPLUGINARRAY_T */
#ifndef EXTERN_TYPEDEF_SHACHANNEL_T
#define EXTERN_TYPEDEF_SHACHANNEL_T
	typedef void* SHACHANNEL_T;
#endif /* EXTERN_TYPEDEF_SHACHANNEL_T */
#ifndef EXTERN_TYPEDEF_SPECTROMETERSTATUS_T
#define EXTERN_TYPEDEF_SPECTROMETERSTATUS_T
	typedef void* SPECTROMETERSTATUS_T;
#endif /* EXTERN_TYPEDEF_SPECTROMETERSTATUS_T */
#ifndef EXTERN_TYPEDEF_ACQUISITIONLISTENER_T
#define EXTERN_TYPEDEF_ACQUISITIONLISTENER_T
	typedef void* ACQUISITIONLISTENER_T;
#endif /* EXTERN_TYPEDEF_ACQUISITIONLISTENER_T */
#ifndef EXTERN_TYPEDEF_HR4000_T
#define EXTERN_TYPEDEF_HR4000_T
	typedef void* HR4000_T;
#endif /* EXTERN_TYPEDEF_HR4000_T */

	EXPORTED HR4000_T HR4000_Create();
	EXPORTED HR4000_T HR4000_Create_1(int i);
	EXPORTED void HR4000_setEndpoints(HR4000_T c_hr4000);
	EXPORTED void HR4000_getEndpoint(HR4000_T c_hr4000, int endPoint, USBENDPOINTDESCRIPTOR_T retval);
	EXPORTED short HR4000_allowWriteToEEPROM(HR4000_T c_hr4000, int privilegeLevel, int slot);
	EXPORTED void HR4000_openSpectrometer(HR4000_T c_hr4000, int index);
	EXPORTED void HR4000_getFeatureControllerGPIO(HR4000_T c_hr4000, USBFEATURE_T retval);
	EXPORTED void HR4000_getGUIFeatures(HR4000_T c_hr4000, GUIPROVIDERARRAY_T retval);
	EXPORTED void HR4000_readSpectrum(HR4000_T c_hr4000, CHARARRAY_T data);
	EXPORTED void HR4000_readSpectrum_1(HR4000_T c_hr4000);
	EXPORTED int HR4000_readIntegrationTime(HR4000_T c_hr4000);
	EXPORTED void HR4000_setIntegrationTime(HR4000_T c_hr4000, int intTime);
	EXPORTED int HR4000_getGatingModeIntegrationTime(HR4000_T c_hr4000);
	EXPORTED void HR4000_addOmniDriverDispatchListener(HR4000_T c_hr4000, OMNIDRIVERDISPATCHLISTENER_T listener);
	EXPORTED void HR4000_removeOmniDriverDispatchListener(HR4000_T c_hr4000, OMNIDRIVERDISPATCHLISTENER_T listener);
	EXPORTED void HR4000_getName(HR4000_T c_hr4000, JSTRING_T retval);
	EXPORTED short HR4000_isSHAEnabled(HR4000_T c_hr4000);
	EXPORTED void HR4000_enableSHA(HR4000_T c_hr4000, short enable);
	EXPORTED void HR4000_setStrobeDelay(HR4000_T c_hr4000, int delay);
	EXPORTED void HR4000_toString(HR4000_T c_hr4000, JSTRING_T retval);
	EXPORTED void HR4000_setAdvancedIntegrationTime(HR4000_T c_hr4000, long long delayMicros);
	EXPORTED int HR4000_getIntegrationTimeBaseClock(HR4000_T c_hr4000);
	EXPORTED int HR4000_getIntegrationClockTimer(HR4000_T c_hr4000);
	EXPORTED long long HR4000_getAdvancedIntegrationTimeMinimum(HR4000_T c_hr4000);
	EXPORTED long long HR4000_getAdvancedIntegrationTimeMaximum(HR4000_T c_hr4000);
	EXPORTED long long HR4000_getAdvancedIntegrationTimeIncrement(HR4000_T c_hr4000);
	EXPORTED void HR4000_getPSOCVersion(HR4000_T c_hr4000, JSTRING_T retval);
	EXPORTED void HR4000_getFPGAFirmwareVersion(HR4000_T c_hr4000, JSTRING_T retval);
	EXPORTED double HR4000_getVoltageIn(HR4000_T c_hr4000);
	EXPORTED void HR4000_setDACCounts(HR4000_T c_hr4000, int counts, int index);
	EXPORTED double HR4000_analogOutCountsToVolts(HR4000_T c_hr4000, int counts);
	EXPORTED int HR4000_getDACMinimum(HR4000_T c_hr4000);
	EXPORTED int HR4000_getDACMaximum(HR4000_T c_hr4000);
	EXPORTED int HR4000_getDACIncrement(HR4000_T c_hr4000);
	EXPORTED short HR4000_isDACPresent(HR4000_T c_hr4000);
	EXPORTED int HR4000_getDACPins(HR4000_T c_hr4000);
	EXPORTED double HR4000_getBoardTemperatureCelsius(HR4000_T c_hr4000);
	EXPORTED void HR4000_setContinuousStrobeDelay(HR4000_T c_hr4000, int delayMicros);
	EXPORTED int HR4000_getContinuousStrobeDelayMinimum(HR4000_T c_hr4000);
	EXPORTED int HR4000_getContinuousStrobeDelayMaximum(HR4000_T c_hr4000);
	EXPORTED int HR4000_getContinuousStrobeDelayIncrement(HR4000_T c_hr4000, int magnitude);
	EXPORTED double HR4000_continuousStrobeCountsToMicros(HR4000_T c_hr4000, int counts);
	EXPORTED void HR4000_setContinuousModeType(HR4000_T c_hr4000, short mode);
	EXPORTED void HR4000_setDelayAfterIntegration(HR4000_T c_hr4000, int delay);
	EXPORTED short HR4000_getContinuousModeType(HR4000_T c_hr4000);
	EXPORTED int HR4000_getDelayAfterIntegration(HR4000_T c_hr4000);
	EXPORTED void HR4000_setContinuousEnable(HR4000_T c_hr4000, short value);
	EXPORTED short HR4000_getContinuousEnable(HR4000_T c_hr4000);
	EXPORTED void HR4000_setExternalTriggerDelay(HR4000_T c_hr4000, int counts);
	EXPORTED double HR4000_triggerDelayCountsToMicroseconds(HR4000_T c_hr4000, int counts);
	EXPORTED int HR4000_getExternalTriggerDelayMinimum(HR4000_T c_hr4000);
	EXPORTED int HR4000_getExternalTriggerDelayMaximum(HR4000_T c_hr4000);
	EXPORTED int HR4000_getExternalTriggerDelayIncrement(HR4000_T c_hr4000);
	EXPORTED void HR4000_setDirectionAllBits(HR4000_T c_hr4000, BITSET_T bitSet);
	EXPORTED void HR4000_setMuxAllBits(HR4000_T c_hr4000, BITSET_T bitSet);
	EXPORTED void HR4000_setValueAllBits(HR4000_T c_hr4000, BITSET_T bitSet);
	EXPORTED void HR4000_setDirectionBitmask(HR4000_T c_hr4000, short bitmask);
	EXPORTED void HR4000_setMuxBitmask(HR4000_T c_hr4000, short bitmask);
	EXPORTED void HR4000_setValueBitmask(HR4000_T c_hr4000, short bitmask);
	EXPORTED void HR4000_setDirectionBit(HR4000_T c_hr4000, int bit, short value);
	EXPORTED void HR4000_setMuxBit(HR4000_T c_hr4000, int bit, short value);
	EXPORTED void HR4000_setValueBit(HR4000_T c_hr4000, int bit, short value);
	EXPORTED int HR4000_getTotalGPIOBits(HR4000_T c_hr4000);
	EXPORTED void HR4000_getDirectionBits(HR4000_T c_hr4000, BITSET_T retval);
	EXPORTED void HR4000_getMuxBits(HR4000_T c_hr4000, BITSET_T retval);
	EXPORTED int HR4000_getValueBit(HR4000_T c_hr4000, int bitNumber);
	EXPORTED void HR4000_getValueBits(HR4000_T c_hr4000, BITSET_T retval);
	EXPORTED int HR4000_getNumberOfPins(HR4000_T c_hr4000);
	EXPORTED void HR4000_setExternalTriggerMode(HR4000_T c_hr4000, int mode);
	EXPORTED void HR4000_getExternalTriggerModes(HR4000_T c_hr4000, EXTERNALTRIGGERMODEARRAY_T retval);
	EXPORTED int HR4000_setI2CBytes(HR4000_T c_hr4000, signed char address, signed char numBytes, CHARARRAY_T i2C);
	EXPORTED void HR4000_getI2CBytes(HR4000_T c_hr4000, signed char address, signed char numBytes, CHARARRAY_T retval);
	EXPORTED int HR4000_setSHAI2CBytes(HR4000_T c_hr4000, signed char address, signed char numBytes, CHARARRAY_T i2C);
	EXPORTED void HR4000_getSHAI2CBytes(HR4000_T c_hr4000, signed char address, signed char numBytes, CHARARRAY_T retval);
	EXPORTED void HR4000_getIrradianceCalibrationFactors(HR4000_T c_hr4000, DOUBLEARRAY_T retval);
	EXPORTED void HR4000_setIrradianceCalibrationFactors(HR4000_T c_hr4000, DOUBLEARRAY_T data);
	EXPORTED double HR4000_getCollectionArea(HR4000_T c_hr4000);
	EXPORTED short HR4000_hasCollectionArea(HR4000_T c_hr4000);
	EXPORTED void HR4000_setCollectionArea(HR4000_T c_hr4000, double area);
	EXPORTED void HR4000_setMasterClockDivisor(HR4000_T c_hr4000, int value);
	EXPORTED int HR4000_getMasterClockDivisor(HR4000_T c_hr4000);
	EXPORTED void HR4000_readNonlinearityCoefficientsFromSpectrometer(HR4000_T c_hr4000, COEFFICIENTSARRAY_T retval);
	EXPORTED void HR4000_writeNonlinearityCoefficientsToSpectrometer(HR4000_T c_hr4000, COEFFICIENTSARRAY_T coefficients);
	EXPORTED void HR4000_getNonlinearityCoefficients(HR4000_T c_hr4000, COEFFICIENTSARRAY_T retval);
	EXPORTED void HR4000_setNonlinearityCoefficients(HR4000_T c_hr4000, COEFFICIENTSARRAY_T coefficients);
	EXPORTED void HR4000_getNonlinearityCoefficientsSingleChannel(HR4000_T c_hr4000, int index, DOUBLEARRAY_T retval);
	EXPORTED void HR4000_setNonlinearityCoefficientsSingleChannel(HR4000_T c_hr4000, DOUBLEARRAY_T nl, int index);
	EXPORTED void HR4000_getPlugIns(HR4000_T c_hr4000, SPECTROMETERPLUGINARRAY_T retval);
	EXPORTED int HR4000_getNumberOfPlugIns(HR4000_T c_hr4000);
	EXPORTED short HR4000_isPlugInDetected(HR4000_T c_hr4000, int id);
	EXPORTED void HR4000_initializePlugIns(HR4000_T c_hr4000, CHARARRAY_T retval);
	EXPORTED void HR4000_detectPlugIns(HR4000_T c_hr4000);
	EXPORTED short HR4000_isHyperAdapterPresent(HR4000_T c_hr4000);
	EXPORTED void HR4000_initSHA(HR4000_T c_hr4000, SHACHANNEL_T channel);
	EXPORTED int HR4000_setHyperAdapterDAC(HR4000_T c_hr4000, double voltage);
	EXPORTED int HR4000_readHyperAdapterEEPROM(HR4000_T c_hr4000, CHARARRAY_T data, int start, int length);
	EXPORTED int HR4000_writeHyperAdapterEEPROM(HR4000_T c_hr4000, CHARARRAY_T data, int start, int length);
	EXPORTED void HR4000_calibrate(HR4000_T c_hr4000);
	EXPORTED double HR4000_getCalibrationTime(HR4000_T c_hr4000);
	EXPORTED void HR4000_restoreCalibration(HR4000_T c_hr4000);
	EXPORTED void HR4000_getSHAChannel(HR4000_T c_hr4000, SHACHANNEL_T retval);
	EXPORTED void HR4000_setShutterClock(HR4000_T c_hr4000, int value);
	EXPORTED int HR4000_getShutterClock(HR4000_T c_hr4000);
	EXPORTED void HR4000_setSingleStrobeLow(HR4000_T c_hr4000, int value);
	EXPORTED void HR4000_setSingleStrobeHigh(HR4000_T c_hr4000, int value);
	EXPORTED double HR4000_getSingleStrobeCountsToMicros(HR4000_T c_hr4000, int counts);
	EXPORTED int HR4000_getSingleStrobeLow(HR4000_T c_hr4000);
	EXPORTED int HR4000_getSingleStrobeHigh(HR4000_T c_hr4000);
	EXPORTED int HR4000_getSingleStrobeMinimum(HR4000_T c_hr4000);
	EXPORTED int HR4000_getSingleStrobeMaximum(HR4000_T c_hr4000);
	EXPORTED int HR4000_getSingleStrobeIncrement(HR4000_T c_hr4000);
	EXPORTED void HR4000_getSPIBytes(HR4000_T c_hr4000, CHARARRAY_T message, int length, CHARARRAY_T retval);
	EXPORTED void HR4000_getStatus(HR4000_T c_hr4000, SPECTROMETERSTATUS_T retval);
	EXPORTED void HR4000_readStrayLightCorrectionCoefficientFromSpectrometer(HR4000_T c_hr4000, COEFFICIENTSARRAY_T retval);
	EXPORTED void HR4000_writeStrayLightCoefficientToSpectrometer(HR4000_T c_hr4000, COEFFICIENTSARRAY_T coefficients);
	EXPORTED void HR4000_setStrayLightCorrectionCoefficient(HR4000_T c_hr4000, COEFFICIENTSARRAY_T coefficients);
	EXPORTED void HR4000_getStrayLightCorrectionCoefficient(HR4000_T c_hr4000, COEFFICIENTSARRAY_T retval);
	EXPORTED void HR4000_setStrayLight(HR4000_T c_hr4000, double strayLight, int index);
	EXPORTED double HR4000_getStrayLight(HR4000_T c_hr4000, int index);
	EXPORTED void HR4000_readWavelengthCalibrationCoefficientsFromSpectrometer(HR4000_T c_hr4000, COEFFICIENTSARRAY_T retval);
	EXPORTED void HR4000_writeWavelengthCoefficientsToSpectrometer(HR4000_T c_hr4000, COEFFICIENTSARRAY_T coefficients);
	EXPORTED void HR4000_getWavelengthCalibrationCoefficients(HR4000_T c_hr4000, COEFFICIENTSARRAY_T retval);
	EXPORTED void HR4000_setWavelengthCalibrationCoefficients(HR4000_T c_hr4000, COEFFICIENTSARRAY_T coefficients);
	EXPORTED void HR4000_getWavelengths(HR4000_T c_hr4000, int index, DOUBLEARRAY_T retval);
	EXPORTED void HR4000_setWavelengths(HR4000_T c_hr4000, DOUBLEARRAY_T wl, int index);
	EXPORTED short HR4000_isAdvancedVersion(HR4000_T c_hr4000);
	EXPORTED void HR4000_addAcquisitionListener(HR4000_T c_hr4000, ACQUISITIONLISTENER_T listener);
	EXPORTED void HR4000_removeAcquisitionListener(HR4000_T c_hr4000, ACQUISITIONLISTENER_T listener);
	EXPORTED void HR4000_Destroy(HR4000_T hr4000);

#ifdef WIN32
	// Use the following function prototypes when calling functions from Visual Basic 6
	EXPORTED HR4000_T STDCALL HR4000_Create_stdcall();
	EXPORTED HR4000_T STDCALL HR4000_Create_stdcall_1(int i);
	EXPORTED void STDCALL HR4000_setEndpoints_stdcall(HR4000_T c_hr4000);
	EXPORTED void STDCALL HR4000_getEndpoint_stdcall(HR4000_T c_hr4000, int endPoint, USBENDPOINTDESCRIPTOR_T retval);
	EXPORTED short STDCALL HR4000_allowWriteToEEPROM_stdcall(HR4000_T c_hr4000, int privilegeLevel, int slot);
	EXPORTED void STDCALL HR4000_openSpectrometer_stdcall(HR4000_T c_hr4000, int index);
	EXPORTED void STDCALL HR4000_getFeatureControllerGPIO_stdcall(HR4000_T c_hr4000, USBFEATURE_T retval);
	EXPORTED void STDCALL HR4000_getGUIFeatures_stdcall(HR4000_T c_hr4000, GUIPROVIDERARRAY_T retval);
	EXPORTED void STDCALL HR4000_readSpectrum_stdcall(HR4000_T c_hr4000, CHARARRAY_T data);
	EXPORTED void STDCALL HR4000_readSpectrum_stdcall_1(HR4000_T c_hr4000);
	EXPORTED int STDCALL HR4000_readIntegrationTime_stdcall(HR4000_T c_hr4000);
	EXPORTED void STDCALL HR4000_setIntegrationTime_stdcall(HR4000_T c_hr4000, int intTime);
	EXPORTED int STDCALL HR4000_getGatingModeIntegrationTime_stdcall(HR4000_T c_hr4000);
	EXPORTED void STDCALL HR4000_addOmniDriverDispatchListener_stdcall(HR4000_T c_hr4000, OMNIDRIVERDISPATCHLISTENER_T listener);
	EXPORTED void STDCALL HR4000_removeOmniDriverDispatchListener_stdcall(HR4000_T c_hr4000, OMNIDRIVERDISPATCHLISTENER_T listener);
	EXPORTED void STDCALL HR4000_getName_stdcall(HR4000_T c_hr4000, JSTRING_T retval);
	EXPORTED short STDCALL HR4000_isSHAEnabled_stdcall(HR4000_T c_hr4000);
	EXPORTED void STDCALL HR4000_enableSHA_stdcall(HR4000_T c_hr4000, short enable);
	EXPORTED void STDCALL HR4000_setStrobeDelay_stdcall(HR4000_T c_hr4000, int delay);
	EXPORTED void STDCALL HR4000_toString_stdcall(HR4000_T c_hr4000, JSTRING_T retval);
	EXPORTED void STDCALL HR4000_setAdvancedIntegrationTime_stdcall(HR4000_T c_hr4000, long long delayMicros);
	EXPORTED int STDCALL HR4000_getIntegrationTimeBaseClock_stdcall(HR4000_T c_hr4000);
	EXPORTED int STDCALL HR4000_getIntegrationClockTimer_stdcall(HR4000_T c_hr4000);
	EXPORTED long long STDCALL HR4000_getAdvancedIntegrationTimeMinimum_stdcall(HR4000_T c_hr4000);
	EXPORTED long long STDCALL HR4000_getAdvancedIntegrationTimeMaximum_stdcall(HR4000_T c_hr4000);
	EXPORTED long long STDCALL HR4000_getAdvancedIntegrationTimeIncrement_stdcall(HR4000_T c_hr4000);
	EXPORTED void STDCALL HR4000_getPSOCVersion_stdcall(HR4000_T c_hr4000, JSTRING_T retval);
	EXPORTED void STDCALL HR4000_getFPGAFirmwareVersion_stdcall(HR4000_T c_hr4000, JSTRING_T retval);
	EXPORTED double STDCALL HR4000_getVoltageIn_stdcall(HR4000_T c_hr4000);
	EXPORTED void STDCALL HR4000_setDACCounts_stdcall(HR4000_T c_hr4000, int counts, int index);
	EXPORTED double STDCALL HR4000_analogOutCountsToVolts_stdcall(HR4000_T c_hr4000, int counts);
	EXPORTED int STDCALL HR4000_getDACMinimum_stdcall(HR4000_T c_hr4000);
	EXPORTED int STDCALL HR4000_getDACMaximum_stdcall(HR4000_T c_hr4000);
	EXPORTED int STDCALL HR4000_getDACIncrement_stdcall(HR4000_T c_hr4000);
	EXPORTED short STDCALL HR4000_isDACPresent_stdcall(HR4000_T c_hr4000);
	EXPORTED int STDCALL HR4000_getDACPins_stdcall(HR4000_T c_hr4000);
	EXPORTED double STDCALL HR4000_getBoardTemperatureCelsius_stdcall(HR4000_T c_hr4000);
	EXPORTED void STDCALL HR4000_setContinuousStrobeDelay_stdcall(HR4000_T c_hr4000, int delayMicros);
	EXPORTED int STDCALL HR4000_getContinuousStrobeDelayMinimum_stdcall(HR4000_T c_hr4000);
	EXPORTED int STDCALL HR4000_getContinuousStrobeDelayMaximum_stdcall(HR4000_T c_hr4000);
	EXPORTED int STDCALL HR4000_getContinuousStrobeDelayIncrement_stdcall(HR4000_T c_hr4000, int magnitude);
	EXPORTED double STDCALL HR4000_continuousStrobeCountsToMicros_stdcall(HR4000_T c_hr4000, int counts);
	EXPORTED void STDCALL HR4000_setContinuousModeType_stdcall(HR4000_T c_hr4000, short mode);
	EXPORTED void STDCALL HR4000_setDelayAfterIntegration_stdcall(HR4000_T c_hr4000, int delay);
	EXPORTED short STDCALL HR4000_getContinuousModeType_stdcall(HR4000_T c_hr4000);
	EXPORTED int STDCALL HR4000_getDelayAfterIntegration_stdcall(HR4000_T c_hr4000);
	EXPORTED void STDCALL HR4000_setContinuousEnable_stdcall(HR4000_T c_hr4000, short value);
	EXPORTED short STDCALL HR4000_getContinuousEnable_stdcall(HR4000_T c_hr4000);
	EXPORTED void STDCALL HR4000_setExternalTriggerDelay_stdcall(HR4000_T c_hr4000, int counts);
	EXPORTED double STDCALL HR4000_triggerDelayCountsToMicroseconds_stdcall(HR4000_T c_hr4000, int counts);
	EXPORTED int STDCALL HR4000_getExternalTriggerDelayMinimum_stdcall(HR4000_T c_hr4000);
	EXPORTED int STDCALL HR4000_getExternalTriggerDelayMaximum_stdcall(HR4000_T c_hr4000);
	EXPORTED int STDCALL HR4000_getExternalTriggerDelayIncrement_stdcall(HR4000_T c_hr4000);
	EXPORTED void STDCALL HR4000_setDirectionAllBits_stdcall(HR4000_T c_hr4000, BITSET_T bitSet);
	EXPORTED void STDCALL HR4000_setMuxAllBits_stdcall(HR4000_T c_hr4000, BITSET_T bitSet);
	EXPORTED void STDCALL HR4000_setValueAllBits_stdcall(HR4000_T c_hr4000, BITSET_T bitSet);
	EXPORTED void STDCALL HR4000_setDirectionBitmask_stdcall(HR4000_T c_hr4000, short bitmask);
	EXPORTED void STDCALL HR4000_setMuxBitmask_stdcall(HR4000_T c_hr4000, short bitmask);
	EXPORTED void STDCALL HR4000_setValueBitmask_stdcall(HR4000_T c_hr4000, short bitmask);
	EXPORTED void STDCALL HR4000_setDirectionBit_stdcall(HR4000_T c_hr4000, int bit, short value);
	EXPORTED void STDCALL HR4000_setMuxBit_stdcall(HR4000_T c_hr4000, int bit, short value);
	EXPORTED void STDCALL HR4000_setValueBit_stdcall(HR4000_T c_hr4000, int bit, short value);
	EXPORTED int STDCALL HR4000_getTotalGPIOBits_stdcall(HR4000_T c_hr4000);
	EXPORTED void STDCALL HR4000_getDirectionBits_stdcall(HR4000_T c_hr4000, BITSET_T retval);
	EXPORTED void STDCALL HR4000_getMuxBits_stdcall(HR4000_T c_hr4000, BITSET_T retval);
	EXPORTED int STDCALL HR4000_getValueBit_stdcall(HR4000_T c_hr4000, int bitNumber);
	EXPORTED void STDCALL HR4000_getValueBits_stdcall(HR4000_T c_hr4000, BITSET_T retval);
	EXPORTED int STDCALL HR4000_getNumberOfPins_stdcall(HR4000_T c_hr4000);
	EXPORTED void STDCALL HR4000_setExternalTriggerMode_stdcall(HR4000_T c_hr4000, int mode);
	EXPORTED void STDCALL HR4000_getExternalTriggerModes_stdcall(HR4000_T c_hr4000, EXTERNALTRIGGERMODEARRAY_T retval);
	EXPORTED int STDCALL HR4000_setI2CBytes_stdcall(HR4000_T c_hr4000, signed char address, signed char numBytes, CHARARRAY_T i2C);
	EXPORTED void STDCALL HR4000_getI2CBytes_stdcall(HR4000_T c_hr4000, signed char address, signed char numBytes, CHARARRAY_T retval);
	EXPORTED int STDCALL HR4000_setSHAI2CBytes_stdcall(HR4000_T c_hr4000, signed char address, signed char numBytes, CHARARRAY_T i2C);
	EXPORTED void STDCALL HR4000_getSHAI2CBytes_stdcall(HR4000_T c_hr4000, signed char address, signed char numBytes, CHARARRAY_T retval);
	EXPORTED void STDCALL HR4000_getIrradianceCalibrationFactors_stdcall(HR4000_T c_hr4000, DOUBLEARRAY_T retval);
	EXPORTED void STDCALL HR4000_setIrradianceCalibrationFactors_stdcall(HR4000_T c_hr4000, DOUBLEARRAY_T data);
	EXPORTED double STDCALL HR4000_getCollectionArea_stdcall(HR4000_T c_hr4000);
	EXPORTED short STDCALL HR4000_hasCollectionArea_stdcall(HR4000_T c_hr4000);
	EXPORTED void STDCALL HR4000_setCollectionArea_stdcall(HR4000_T c_hr4000, double area);
	EXPORTED void STDCALL HR4000_setMasterClockDivisor_stdcall(HR4000_T c_hr4000, int value);
	EXPORTED int STDCALL HR4000_getMasterClockDivisor_stdcall(HR4000_T c_hr4000);
	EXPORTED void STDCALL HR4000_readNonlinearityCoefficientsFromSpectrometer_stdcall(HR4000_T c_hr4000, COEFFICIENTSARRAY_T retval);
	EXPORTED void STDCALL HR4000_writeNonlinearityCoefficientsToSpectrometer_stdcall(HR4000_T c_hr4000, COEFFICIENTSARRAY_T coefficients);
	EXPORTED void STDCALL HR4000_getNonlinearityCoefficients_stdcall(HR4000_T c_hr4000, COEFFICIENTSARRAY_T retval);
	EXPORTED void STDCALL HR4000_setNonlinearityCoefficients_stdcall(HR4000_T c_hr4000, COEFFICIENTSARRAY_T coefficients);
	EXPORTED void STDCALL HR4000_getNonlinearityCoefficientsSingleChannel_stdcall(HR4000_T c_hr4000, int index, DOUBLEARRAY_T retval);
	EXPORTED void STDCALL HR4000_setNonlinearityCoefficientsSingleChannel_stdcall(HR4000_T c_hr4000, DOUBLEARRAY_T nl, int index);
	EXPORTED void STDCALL HR4000_getPlugIns_stdcall(HR4000_T c_hr4000, SPECTROMETERPLUGINARRAY_T retval);
	EXPORTED int STDCALL HR4000_getNumberOfPlugIns_stdcall(HR4000_T c_hr4000);
	EXPORTED short STDCALL HR4000_isPlugInDetected_stdcall(HR4000_T c_hr4000, int id);
	EXPORTED void STDCALL HR4000_initializePlugIns_stdcall(HR4000_T c_hr4000, CHARARRAY_T retval);
	EXPORTED void STDCALL HR4000_detectPlugIns_stdcall(HR4000_T c_hr4000);
	EXPORTED short STDCALL HR4000_isHyperAdapterPresent_stdcall(HR4000_T c_hr4000);
	EXPORTED void STDCALL HR4000_initSHA_stdcall(HR4000_T c_hr4000, SHACHANNEL_T channel);
	EXPORTED int STDCALL HR4000_setHyperAdapterDAC_stdcall(HR4000_T c_hr4000, double voltage);
	EXPORTED int STDCALL HR4000_readHyperAdapterEEPROM_stdcall(HR4000_T c_hr4000, CHARARRAY_T data, int start, int length);
	EXPORTED int STDCALL HR4000_writeHyperAdapterEEPROM_stdcall(HR4000_T c_hr4000, CHARARRAY_T data, int start, int length);
	EXPORTED void STDCALL HR4000_calibrate_stdcall(HR4000_T c_hr4000);
	EXPORTED double STDCALL HR4000_getCalibrationTime_stdcall(HR4000_T c_hr4000);
	EXPORTED void STDCALL HR4000_restoreCalibration_stdcall(HR4000_T c_hr4000);
	EXPORTED void STDCALL HR4000_getSHAChannel_stdcall(HR4000_T c_hr4000, SHACHANNEL_T retval);
	EXPORTED void STDCALL HR4000_setShutterClock_stdcall(HR4000_T c_hr4000, int value);
	EXPORTED int STDCALL HR4000_getShutterClock_stdcall(HR4000_T c_hr4000);
	EXPORTED void STDCALL HR4000_setSingleStrobeLow_stdcall(HR4000_T c_hr4000, int value);
	EXPORTED void STDCALL HR4000_setSingleStrobeHigh_stdcall(HR4000_T c_hr4000, int value);
	EXPORTED double STDCALL HR4000_getSingleStrobeCountsToMicros_stdcall(HR4000_T c_hr4000, int counts);
	EXPORTED int STDCALL HR4000_getSingleStrobeLow_stdcall(HR4000_T c_hr4000);
	EXPORTED int STDCALL HR4000_getSingleStrobeHigh_stdcall(HR4000_T c_hr4000);
	EXPORTED int STDCALL HR4000_getSingleStrobeMinimum_stdcall(HR4000_T c_hr4000);
	EXPORTED int STDCALL HR4000_getSingleStrobeMaximum_stdcall(HR4000_T c_hr4000);
	EXPORTED int STDCALL HR4000_getSingleStrobeIncrement_stdcall(HR4000_T c_hr4000);
	EXPORTED void STDCALL HR4000_getSPIBytes_stdcall(HR4000_T c_hr4000, CHARARRAY_T message, int length, CHARARRAY_T retval);
	EXPORTED void STDCALL HR4000_getStatus_stdcall(HR4000_T c_hr4000, SPECTROMETERSTATUS_T retval);
	EXPORTED void STDCALL HR4000_readStrayLightCorrectionCoefficientFromSpectrometer_stdcall(HR4000_T c_hr4000, COEFFICIENTSARRAY_T retval);
	EXPORTED void STDCALL HR4000_writeStrayLightCoefficientToSpectrometer_stdcall(HR4000_T c_hr4000, COEFFICIENTSARRAY_T coefficients);
	EXPORTED void STDCALL HR4000_setStrayLightCorrectionCoefficient_stdcall(HR4000_T c_hr4000, COEFFICIENTSARRAY_T coefficients);
	EXPORTED void STDCALL HR4000_getStrayLightCorrectionCoefficient_stdcall(HR4000_T c_hr4000, COEFFICIENTSARRAY_T retval);
	EXPORTED void STDCALL HR4000_setStrayLight_stdcall(HR4000_T c_hr4000, double strayLight, int index);
	EXPORTED double STDCALL HR4000_getStrayLight_stdcall(HR4000_T c_hr4000, int index);
	EXPORTED void STDCALL HR4000_readWavelengthCalibrationCoefficientsFromSpectrometer_stdcall(HR4000_T c_hr4000, COEFFICIENTSARRAY_T retval);
	EXPORTED void STDCALL HR4000_writeWavelengthCoefficientsToSpectrometer_stdcall(HR4000_T c_hr4000, COEFFICIENTSARRAY_T coefficients);
	EXPORTED void STDCALL HR4000_getWavelengthCalibrationCoefficients_stdcall(HR4000_T c_hr4000, COEFFICIENTSARRAY_T retval);
	EXPORTED void STDCALL HR4000_setWavelengthCalibrationCoefficients_stdcall(HR4000_T c_hr4000, COEFFICIENTSARRAY_T coefficients);
	EXPORTED void STDCALL HR4000_getWavelengths_stdcall(HR4000_T c_hr4000, int index, DOUBLEARRAY_T retval);
	EXPORTED void STDCALL HR4000_setWavelengths_stdcall(HR4000_T c_hr4000, DOUBLEARRAY_T wl, int index);
	EXPORTED short STDCALL HR4000_isAdvancedVersion_stdcall(HR4000_T c_hr4000);
	EXPORTED void STDCALL HR4000_addAcquisitionListener_stdcall(HR4000_T c_hr4000, ACQUISITIONLISTENER_T listener);
	EXPORTED void STDCALL HR4000_removeAcquisitionListener_stdcall(HR4000_T c_hr4000, ACQUISITIONLISTENER_T listener);
	EXPORTED void STDCALL HR4000_Destroy_stdcall(HR4000_T hr4000);
#endif /* WIN32 */
#ifdef __cplusplus
};
#endif /* __cplusplus */
#endif /* HR4000_H */
