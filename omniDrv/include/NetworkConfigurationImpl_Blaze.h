/**
 * File: NetworkConfigurationImpl_Blaze.h
 * Autogenerated on Sun Feb 28 18:02:24 EST 2016 based on
 * NetworkConfigurationImpl_Blaze.java
 * for the Java class
 * com/oceanoptics/omnidriver/features/network/NetworkConfigurationImpl_Blaze
 * Copyright (C) 2006 - 2008 Ocean Optics, Inc.  All rights reserved.
 */


#ifndef NETWORKCONFIGURATIONIMPL_BLAZE_H
#define NETWORKCONFIGURATIONIMPL_BLAZE_H
#include "BaseJavaClass.h"

#ifdef __cplusplus

#ifdef INCLUDES_IN_HEADER
#include "NetworkConfigurationImpl.h"
#include "USBInterface.h"
#include "BlazeUSB.h"
#include "EthernetMACAddress.h"
#endif /* INCLUDES_IN_HEADER */

/* Pre-declarations for circular dependencies in header files */
#ifdef CLASS_PREDECLARATIONS
class NetworkConfigurationImpl;
class USBInterface;
class BlazeUSB;
class EthernetMACAddress;
#endif /* CLASS_PREDECLARATIONS */

CLASS_TOKEN EXPORTED NetworkConfigurationImpl_Blaze : public NetworkConfigurationImpl // CPPClass.tag001
{
public: 
	NetworkConfigurationImpl_Blaze(USBInterface& usbInt, BlazeUSB& s);
	void writeMACAddressToSpectrometer(EthernetMACAddress& emac);
	EthernetMACAddress readMACAddressFromSpectrometer(int iface);
	~NetworkConfigurationImpl_Blaze();
	// No public default Java constructor; creating one:
	NetworkConfigurationImpl_Blaze();
	// No public Java copy constructor; creating one:
	NetworkConfigurationImpl_Blaze(const NetworkConfigurationImpl_Blaze &that);
	// Creating assignment operator declaration:
	NetworkConfigurationImpl_Blaze &operator=(const NetworkConfigurationImpl_Blaze &that);

private:
	jmethodID mid_NetworkConfigurationImpl_Blaze1436;
	jmethodID mid_writeMACAddressToSpectrometer1437;
	jmethodID mid_readMACAddressFromSpectrometer1438;
	void init_ids(JNIEnv* pJNIEnv);
};

extern "C" {
#endif /* __cplusplus */
#ifndef EXTERN_TYPEDEF_NETWORKCONFIGURATIONIMPL_T
#define EXTERN_TYPEDEF_NETWORKCONFIGURATIONIMPL_T
	typedef void* NETWORKCONFIGURATIONIMPL_T;
#endif /* EXTERN_TYPEDEF_NETWORKCONFIGURATIONIMPL_T */
#ifndef EXTERN_TYPEDEF_USBINTERFACE_T
#define EXTERN_TYPEDEF_USBINTERFACE_T
	typedef void* USBINTERFACE_T;
#endif /* EXTERN_TYPEDEF_USBINTERFACE_T */
#ifndef EXTERN_TYPEDEF_BLAZEUSB_T
#define EXTERN_TYPEDEF_BLAZEUSB_T
	typedef void* BLAZEUSB_T;
#endif /* EXTERN_TYPEDEF_BLAZEUSB_T */
#ifndef EXTERN_TYPEDEF_ETHERNETMACADDRESS_T
#define EXTERN_TYPEDEF_ETHERNETMACADDRESS_T
	typedef void* ETHERNETMACADDRESS_T;
#endif /* EXTERN_TYPEDEF_ETHERNETMACADDRESS_T */
#ifndef EXTERN_TYPEDEF_NETWORKCONFIGURATIONIMPL_BLAZE_T
#define EXTERN_TYPEDEF_NETWORKCONFIGURATIONIMPL_BLAZE_T
	typedef void* NETWORKCONFIGURATIONIMPL_BLAZE_T;
#endif /* EXTERN_TYPEDEF_NETWORKCONFIGURATIONIMPL_BLAZE_T */

	// No public default Java constructor; creating one:
	EXPORTED NETWORKCONFIGURATIONIMPL_BLAZE_T NetworkConfigurationImpl_Blaze_Create();
	EXPORTED NETWORKCONFIGURATIONIMPL_BLAZE_T NetworkConfigurationImpl_Blaze_Create_1(USBINTERFACE_T usbInt, BLAZEUSB_T s);
	EXPORTED void NetworkConfigurationImpl_Blaze_writeMACAddressToSpectrometer(NETWORKCONFIGURATIONIMPL_BLAZE_T c_network_configuration_impl_blaze, ETHERNETMACADDRESS_T emac);
	EXPORTED void NetworkConfigurationImpl_Blaze_readMACAddressFromSpectrometer(NETWORKCONFIGURATIONIMPL_BLAZE_T c_network_configuration_impl_blaze, int iface, ETHERNETMACADDRESS_T retval);
	EXPORTED void NetworkConfigurationImpl_Blaze_Destroy(NETWORKCONFIGURATIONIMPL_BLAZE_T network_configuration_impl_blaze);

#ifdef WIN32
	// Use the following function prototypes when calling functions from Visual Basic 6
	EXPORTED NETWORKCONFIGURATIONIMPL_BLAZE_T STDCALL NetworkConfigurationImpl_Blaze_Create_stdcall_1(USBINTERFACE_T usbInt, BLAZEUSB_T s);
	EXPORTED void STDCALL NetworkConfigurationImpl_Blaze_writeMACAddressToSpectrometer_stdcall(NETWORKCONFIGURATIONIMPL_BLAZE_T c_network_configuration_impl_blaze, ETHERNETMACADDRESS_T emac);
	EXPORTED void STDCALL NetworkConfigurationImpl_Blaze_readMACAddressFromSpectrometer_stdcall(NETWORKCONFIGURATIONIMPL_BLAZE_T c_network_configuration_impl_blaze, int iface, ETHERNETMACADDRESS_T retval);
	EXPORTED void STDCALL NetworkConfigurationImpl_Blaze_Destroy_stdcall(NETWORKCONFIGURATIONIMPL_BLAZE_T network_configuration_impl_blaze);
#endif /* WIN32 */
#ifdef __cplusplus
};
#endif /* __cplusplus */
#endif /* NETWORKCONFIGURATIONIMPL_BLAZE_H */
