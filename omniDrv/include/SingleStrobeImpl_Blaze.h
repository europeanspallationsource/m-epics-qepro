/**
 * File: SingleStrobeImpl_Blaze.h
 * Autogenerated on Mon Apr 11 10:06:10 EDT 2016 based on
 * SingleStrobeImpl_Blaze.java
 * for the Java class
 * com/oceanoptics/omnidriver/features/singlestrobe/SingleStrobeImpl_Blaze
 * Copyright (C) 2006 - 2008 Ocean Optics, Inc.  All rights reserved.
 */


#ifndef SINGLESTROBEIMPL_BLAZE_H
#define SINGLESTROBEIMPL_BLAZE_H
#include "BaseJavaClass.h"

#ifdef __cplusplus

#ifdef INCLUDES_IN_HEADER
#include "SingleStrobeImpl.h"
#include "OceanBinaryProtocolProvider.h"
#include "USBInterface.h"
#include "JStringArray.h"
#endif /* INCLUDES_IN_HEADER */

/* Pre-declarations for circular dependencies in header files */
#ifdef CLASS_PREDECLARATIONS
class SingleStrobeImpl;
class OceanBinaryProtocolProvider;
class USBInterface;
class JStringArray;
#endif /* CLASS_PREDECLARATIONS */

CLASS_TOKEN EXPORTED SingleStrobeImpl_Blaze : public SingleStrobeImpl // CPPClass.tag001
{
public: 
	SingleStrobeImpl_Blaze(OceanBinaryProtocolProvider& bn, USBInterface& usbInt);
	int getSingleStrobeHigh();
	void setSingleStrobeHigh(int counts);
	int getSingleStrobeLow();
	void setSingleStrobeLow(int counts);
	int getSingleStrobeMinimum();
	int getSingleStrobeMaximum();
	int getSingleStrobeIncrement();
	double getSingleStrobeCountsToMicros(int counts);
	JStringArray getFeatureGUIClassnames();
	~SingleStrobeImpl_Blaze();
	// No public default Java constructor; creating one:
	SingleStrobeImpl_Blaze();
	// No public Java copy constructor; creating one:
	SingleStrobeImpl_Blaze(const SingleStrobeImpl_Blaze &that);
	// Creating assignment operator declaration:
	SingleStrobeImpl_Blaze &operator=(const SingleStrobeImpl_Blaze &that);

private:
	jmethodID mid_SingleStrobeImpl_Blaze1715;
	jmethodID mid_getSingleStrobeHigh1716;
	jmethodID mid_setSingleStrobeHigh1717;
	jmethodID mid_getSingleStrobeLow1718;
	jmethodID mid_setSingleStrobeLow1719;
	jmethodID mid_getSingleStrobeMinimum1720;
	jmethodID mid_getSingleStrobeMaximum1721;
	jmethodID mid_getSingleStrobeIncrement1722;
	jmethodID mid_getSingleStrobeCountsToMicros1723;
	jmethodID mid_getFeatureGUIClassnames1724;
	void init_ids(JNIEnv* pJNIEnv);
};

extern "C" {
#endif /* __cplusplus */
#ifndef EXTERN_TYPEDEF_SINGLESTROBEIMPL_T
#define EXTERN_TYPEDEF_SINGLESTROBEIMPL_T
	typedef void* SINGLESTROBEIMPL_T;
#endif /* EXTERN_TYPEDEF_SINGLESTROBEIMPL_T */
#ifndef EXTERN_TYPEDEF_OCEANBINARYPROTOCOLPROVIDER_T
#define EXTERN_TYPEDEF_OCEANBINARYPROTOCOLPROVIDER_T
	typedef void* OCEANBINARYPROTOCOLPROVIDER_T;
#endif /* EXTERN_TYPEDEF_OCEANBINARYPROTOCOLPROVIDER_T */
#ifndef EXTERN_TYPEDEF_USBINTERFACE_T
#define EXTERN_TYPEDEF_USBINTERFACE_T
	typedef void* USBINTERFACE_T;
#endif /* EXTERN_TYPEDEF_USBINTERFACE_T */
#ifndef EXTERN_TYPEDEF_JSTRINGARRAY_T
#define EXTERN_TYPEDEF_JSTRINGARRAY_T
	typedef void* JSTRINGARRAY_T;
#endif /* EXTERN_TYPEDEF_JSTRINGARRAY_T */
#ifndef EXTERN_TYPEDEF_SINGLESTROBEIMPL_BLAZE_T
#define EXTERN_TYPEDEF_SINGLESTROBEIMPL_BLAZE_T
	typedef void* SINGLESTROBEIMPL_BLAZE_T;
#endif /* EXTERN_TYPEDEF_SINGLESTROBEIMPL_BLAZE_T */

	// No public default Java constructor; creating one:
	EXPORTED SINGLESTROBEIMPL_BLAZE_T SingleStrobeImpl_Blaze_Create();
	EXPORTED SINGLESTROBEIMPL_BLAZE_T SingleStrobeImpl_Blaze_Create_1(OCEANBINARYPROTOCOLPROVIDER_T bn, USBINTERFACE_T usbInt);
	EXPORTED int SingleStrobeImpl_Blaze_getSingleStrobeHigh(SINGLESTROBEIMPL_BLAZE_T c_single_strobe_impl_blaze);
	EXPORTED void SingleStrobeImpl_Blaze_setSingleStrobeHigh(SINGLESTROBEIMPL_BLAZE_T c_single_strobe_impl_blaze, int counts);
	EXPORTED int SingleStrobeImpl_Blaze_getSingleStrobeLow(SINGLESTROBEIMPL_BLAZE_T c_single_strobe_impl_blaze);
	EXPORTED void SingleStrobeImpl_Blaze_setSingleStrobeLow(SINGLESTROBEIMPL_BLAZE_T c_single_strobe_impl_blaze, int counts);
	EXPORTED int SingleStrobeImpl_Blaze_getSingleStrobeMinimum(SINGLESTROBEIMPL_BLAZE_T c_single_strobe_impl_blaze);
	EXPORTED int SingleStrobeImpl_Blaze_getSingleStrobeMaximum(SINGLESTROBEIMPL_BLAZE_T c_single_strobe_impl_blaze);
	EXPORTED int SingleStrobeImpl_Blaze_getSingleStrobeIncrement(SINGLESTROBEIMPL_BLAZE_T c_single_strobe_impl_blaze);
	EXPORTED double SingleStrobeImpl_Blaze_getSingleStrobeCountsToMicros(SINGLESTROBEIMPL_BLAZE_T c_single_strobe_impl_blaze, int counts);
	EXPORTED void SingleStrobeImpl_Blaze_getFeatureGUIClassnames(SINGLESTROBEIMPL_BLAZE_T c_single_strobe_impl_blaze, JSTRINGARRAY_T retval);
	EXPORTED void SingleStrobeImpl_Blaze_Destroy(SINGLESTROBEIMPL_BLAZE_T single_strobe_impl_blaze);

#ifdef WIN32
	// Use the following function prototypes when calling functions from Visual Basic 6
	EXPORTED SINGLESTROBEIMPL_BLAZE_T STDCALL SingleStrobeImpl_Blaze_Create_stdcall_1(OCEANBINARYPROTOCOLPROVIDER_T bn, USBINTERFACE_T usbInt);
	EXPORTED int STDCALL SingleStrobeImpl_Blaze_getSingleStrobeHigh_stdcall(SINGLESTROBEIMPL_BLAZE_T c_single_strobe_impl_blaze);
	EXPORTED void STDCALL SingleStrobeImpl_Blaze_setSingleStrobeHigh_stdcall(SINGLESTROBEIMPL_BLAZE_T c_single_strobe_impl_blaze, int counts);
	EXPORTED int STDCALL SingleStrobeImpl_Blaze_getSingleStrobeLow_stdcall(SINGLESTROBEIMPL_BLAZE_T c_single_strobe_impl_blaze);
	EXPORTED void STDCALL SingleStrobeImpl_Blaze_setSingleStrobeLow_stdcall(SINGLESTROBEIMPL_BLAZE_T c_single_strobe_impl_blaze, int counts);
	EXPORTED int STDCALL SingleStrobeImpl_Blaze_getSingleStrobeMinimum_stdcall(SINGLESTROBEIMPL_BLAZE_T c_single_strobe_impl_blaze);
	EXPORTED int STDCALL SingleStrobeImpl_Blaze_getSingleStrobeMaximum_stdcall(SINGLESTROBEIMPL_BLAZE_T c_single_strobe_impl_blaze);
	EXPORTED int STDCALL SingleStrobeImpl_Blaze_getSingleStrobeIncrement_stdcall(SINGLESTROBEIMPL_BLAZE_T c_single_strobe_impl_blaze);
	EXPORTED double STDCALL SingleStrobeImpl_Blaze_getSingleStrobeCountsToMicros_stdcall(SINGLESTROBEIMPL_BLAZE_T c_single_strobe_impl_blaze, int counts);
	EXPORTED void STDCALL SingleStrobeImpl_Blaze_getFeatureGUIClassnames_stdcall(SINGLESTROBEIMPL_BLAZE_T c_single_strobe_impl_blaze, JSTRINGARRAY_T retval);
	EXPORTED void STDCALL SingleStrobeImpl_Blaze_Destroy_stdcall(SINGLESTROBEIMPL_BLAZE_T single_strobe_impl_blaze);
#endif /* WIN32 */
#ifdef __cplusplus
};
#endif /* __cplusplus */
#endif /* SINGLESTROBEIMPL_BLAZE_H */
