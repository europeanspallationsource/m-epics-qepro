/**
 * File: Spectrometer.h
 * Autogenerated on Mon Apr 11 10:06:16 EDT 2016 based on
 * Spectrometer.java
 * for the Java class
 * com/oceanoptics/omnidriver/spectrometer/Spectrometer
 * Copyright (C) 2006 - 2008 Ocean Optics, Inc.  All rights reserved.
 */


#ifndef SPECTROMETER_H
#define SPECTROMETER_H
#include "BaseJavaClass.h"

#ifdef __cplusplus

#ifdef INCLUDES_IN_HEADER
#include "SpectrumProducerBase.h"
#include "SpectrometerInfo.h"
#include "SpectrometerChannelArray.h"
#include "Coefficients.h"
#include "JString.h"
#endif /* INCLUDES_IN_HEADER */

/* Pre-declarations for circular dependencies in header files */
#ifdef CLASS_PREDECLARATIONS
class SpectrumProducerBase;
class SpectrometerInfo;
class SpectrometerChannelArray;
class Coefficients;
class JString;
#endif /* CLASS_PREDECLARATIONS */

CLASS_TOKEN EXPORTED Spectrometer : public SpectrumProducerBase // CPPClass.tag001
{
public: 
	void openSpectrometer(int param0);
	Spectrometer();
	int getSpectrumReadThrottleMilliseconds();
	void setSpectrumReadThrottleMilliseconds(int value);
	int getSocketTimeoutMilliseconds();
	void setSocketTimeoutMilliseconds(int value);
	short isCheckForBytesAvailableEnabled();
	void setCheckForBytesAvailable(short value);
	int getReadSpectrumRetryLimit();
	void setReadSpectrumRetryLimit(int value);
	short allowWriteToEEPROM(int privilegeLevel, int slot);
	SpectrometerInfo getSpectrumBase();
	void setStabilityScan(short on);
	short isStabilityScan();
	int getIntegrationTimeMinimum();
	int getIntegrationTimeMaximum();
	int getIntegrationTimeIncrement();
	int getIntegrationTimeBase();
	int getActualIntegrationTime();
	SpectrometerChannelArray getChannels();
	int getNumberOfChannels();
	int getNumberOfEnabledChannels();
	IntArray getChannelIndices();
	short isNetworkSpectrometer();
	short isRotatorEnabled();
	void setRotatorEnabled(short rotator);
	int getBenchSlot();
	int getSpectrometerConfigSlot();
	int getDetectorSerialNumberSlot();
	int getCPLDVersionSlot();
	void getConfigurationFromSpectrometer();
	void setConfiguration();
	Coefficients getNewCoefficients(int index);
	void getCoefficientsFromSpectrometer();
	void setCoefficients();
	int getFirmwareVersionNumber();
	JString getCodeVersion(JString& fileName);
	int getMaxIntensity();
	short isStrobeDelayEnabled();
	void close();
	~Spectrometer();
	// No public Java copy constructor; creating one:
	Spectrometer(const Spectrometer &that);
	// Creating assignment operator declaration:
	Spectrometer &operator=(const Spectrometer &that);
	short rotatorEnabled;

private:
	jmethodID mid_openSpectrometer2501;
	jmethodID mid_Spectrometer2502;
	jmethodID mid_getSpectrumReadThrottleMilliseconds2503;
	jmethodID mid_setSpectrumReadThrottleMilliseconds2504;
	jmethodID mid_getSocketTimeoutMilliseconds2505;
	jmethodID mid_setSocketTimeoutMilliseconds2506;
	jmethodID mid_isCheckForBytesAvailableEnabled2507;
	jmethodID mid_setCheckForBytesAvailable2508;
	jmethodID mid_getReadSpectrumRetryLimit2509;
	jmethodID mid_setReadSpectrumRetryLimit2510;
	jmethodID mid_allowWriteToEEPROM2511;
	jmethodID mid_getSpectrumBase2512;
	jmethodID mid_setStabilityScan2513;
	jmethodID mid_isStabilityScan2514;
	jmethodID mid_getIntegrationTimeMinimum2515;
	jmethodID mid_getIntegrationTimeMaximum2516;
	jmethodID mid_getIntegrationTimeIncrement2517;
	jmethodID mid_getIntegrationTimeBase2518;
	jmethodID mid_getActualIntegrationTime2519;
	jmethodID mid_getChannels2520;
	jmethodID mid_getNumberOfChannels2521;
	jmethodID mid_getNumberOfEnabledChannels2522;
	jmethodID mid_getChannelIndices2523;
	jmethodID mid_isNetworkSpectrometer2524;
	jmethodID mid_isRotatorEnabled2525;
	jmethodID mid_setRotatorEnabled2526;
	jmethodID mid_getBenchSlot2527;
	jmethodID mid_getSpectrometerConfigSlot2528;
	jmethodID mid_getDetectorSerialNumberSlot2529;
	jmethodID mid_getCPLDVersionSlot2530;
	jmethodID mid_getConfigurationFromSpectrometer2531;
	jmethodID mid_setConfiguration2532;
	jmethodID mid_getNewCoefficients2533;
	jmethodID mid_getCoefficientsFromSpectrometer2534;
	jmethodID mid_setCoefficients2535;
	jmethodID mid_getFirmwareVersionNumber2536;
	jmethodID mid_getCodeVersion2537;
	jmethodID mid_getMaxIntensity2538;
	jmethodID mid_isStrobeDelayEnabled2539;
	jmethodID mid_close2540;
	jfieldID fid_channels;
	jfieldID fid_rotatorEnabled;
	void init_ids(JNIEnv* pJNIEnv);
};

extern "C" {
#endif /* __cplusplus */
#ifndef EXTERN_TYPEDEF_SPECTRUMPRODUCERBASE_T
#define EXTERN_TYPEDEF_SPECTRUMPRODUCERBASE_T
	typedef void* SPECTRUMPRODUCERBASE_T;
#endif /* EXTERN_TYPEDEF_SPECTRUMPRODUCERBASE_T */
#ifndef EXTERN_TYPEDEF_SPECTROMETERINFO_T
#define EXTERN_TYPEDEF_SPECTROMETERINFO_T
	typedef void* SPECTROMETERINFO_T;
#endif /* EXTERN_TYPEDEF_SPECTROMETERINFO_T */
#ifndef EXTERN_TYPEDEF_SPECTROMETERCHANNELARRAY_T
#define EXTERN_TYPEDEF_SPECTROMETERCHANNELARRAY_T
	typedef void* SPECTROMETERCHANNELARRAY_T;
#endif /* EXTERN_TYPEDEF_SPECTROMETERCHANNELARRAY_T */
#ifndef EXTERN_TYPEDEF_COEFFICIENTS_T
#define EXTERN_TYPEDEF_COEFFICIENTS_T
	typedef void* COEFFICIENTS_T;
#endif /* EXTERN_TYPEDEF_COEFFICIENTS_T */
#ifndef EXTERN_TYPEDEF_JSTRING_T
#define EXTERN_TYPEDEF_JSTRING_T
	typedef void* JSTRING_T;
#endif /* EXTERN_TYPEDEF_JSTRING_T */
#ifndef EXTERN_TYPEDEF_SPECTROMETER_T
#define EXTERN_TYPEDEF_SPECTROMETER_T
	typedef void* SPECTROMETER_T;
#endif /* EXTERN_TYPEDEF_SPECTROMETER_T */

	EXPORTED void Spectrometer_openSpectrometer(SPECTROMETER_T c_spectrometer, int param0);
	EXPORTED SPECTROMETER_T Spectrometer_Create();
	EXPORTED int Spectrometer_getSpectrumReadThrottleMilliseconds(SPECTROMETER_T c_spectrometer);
	EXPORTED void Spectrometer_setSpectrumReadThrottleMilliseconds(SPECTROMETER_T c_spectrometer, int value);
	EXPORTED int Spectrometer_getSocketTimeoutMilliseconds(SPECTROMETER_T c_spectrometer);
	EXPORTED void Spectrometer_setSocketTimeoutMilliseconds(SPECTROMETER_T c_spectrometer, int value);
	EXPORTED short Spectrometer_isCheckForBytesAvailableEnabled(SPECTROMETER_T c_spectrometer);
	EXPORTED void Spectrometer_setCheckForBytesAvailable(SPECTROMETER_T c_spectrometer, short value);
	EXPORTED int Spectrometer_getReadSpectrumRetryLimit(SPECTROMETER_T c_spectrometer);
	EXPORTED void Spectrometer_setReadSpectrumRetryLimit(SPECTROMETER_T c_spectrometer, int value);
	EXPORTED short Spectrometer_allowWriteToEEPROM(SPECTROMETER_T c_spectrometer, int privilegeLevel, int slot);
	EXPORTED void Spectrometer_getSpectrumBase(SPECTROMETER_T c_spectrometer, SPECTROMETERINFO_T retval);
	EXPORTED void Spectrometer_setStabilityScan(SPECTROMETER_T c_spectrometer, short on);
	EXPORTED short Spectrometer_isStabilityScan(SPECTROMETER_T c_spectrometer);
	EXPORTED int Spectrometer_getIntegrationTimeMinimum(SPECTROMETER_T c_spectrometer);
	EXPORTED int Spectrometer_getIntegrationTimeMaximum(SPECTROMETER_T c_spectrometer);
	EXPORTED int Spectrometer_getIntegrationTimeIncrement(SPECTROMETER_T c_spectrometer);
	EXPORTED int Spectrometer_getIntegrationTimeBase(SPECTROMETER_T c_spectrometer);
	EXPORTED int Spectrometer_getActualIntegrationTime(SPECTROMETER_T c_spectrometer);
	EXPORTED void Spectrometer_getChannels(SPECTROMETER_T c_spectrometer, SPECTROMETERCHANNELARRAY_T retval);
	EXPORTED int Spectrometer_getNumberOfChannels(SPECTROMETER_T c_spectrometer);
	EXPORTED int Spectrometer_getNumberOfEnabledChannels(SPECTROMETER_T c_spectrometer);
	EXPORTED void Spectrometer_getChannelIndices(SPECTROMETER_T c_spectrometer, INTARRAY_T retval);
	EXPORTED short Spectrometer_isNetworkSpectrometer(SPECTROMETER_T c_spectrometer);
	EXPORTED short Spectrometer_isRotatorEnabled(SPECTROMETER_T c_spectrometer);
	EXPORTED void Spectrometer_setRotatorEnabled(SPECTROMETER_T c_spectrometer, short rotator);
	EXPORTED int Spectrometer_getBenchSlot(SPECTROMETER_T c_spectrometer);
	EXPORTED int Spectrometer_getSpectrometerConfigSlot(SPECTROMETER_T c_spectrometer);
	EXPORTED int Spectrometer_getDetectorSerialNumberSlot(SPECTROMETER_T c_spectrometer);
	EXPORTED int Spectrometer_getCPLDVersionSlot(SPECTROMETER_T c_spectrometer);
	EXPORTED void Spectrometer_getConfigurationFromSpectrometer(SPECTROMETER_T c_spectrometer);
	EXPORTED void Spectrometer_setConfiguration(SPECTROMETER_T c_spectrometer);
	EXPORTED void Spectrometer_getNewCoefficients(SPECTROMETER_T c_spectrometer, int index, COEFFICIENTS_T retval);
	EXPORTED void Spectrometer_getCoefficientsFromSpectrometer(SPECTROMETER_T c_spectrometer);
	EXPORTED void Spectrometer_setCoefficients(SPECTROMETER_T c_spectrometer);
	EXPORTED int Spectrometer_getFirmwareVersionNumber(SPECTROMETER_T c_spectrometer);
	EXPORTED void Spectrometer_getCodeVersion(SPECTROMETER_T c_spectrometer, JSTRING_T fileName, JSTRING_T retval);
	EXPORTED int Spectrometer_getMaxIntensity(SPECTROMETER_T c_spectrometer);
	EXPORTED short Spectrometer_isStrobeDelayEnabled(SPECTROMETER_T c_spectrometer);
	EXPORTED void Spectrometer_close(SPECTROMETER_T c_spectrometer);
	EXPORTED void Spectrometer_Destroy(SPECTROMETER_T spectrometer);

#ifdef WIN32
	// Use the following function prototypes when calling functions from Visual Basic 6
	EXPORTED void STDCALL Spectrometer_openSpectrometer_stdcall(SPECTROMETER_T c_spectrometer, int param0);
	EXPORTED SPECTROMETER_T STDCALL Spectrometer_Create_stdcall();
	EXPORTED int STDCALL Spectrometer_getSpectrumReadThrottleMilliseconds_stdcall(SPECTROMETER_T c_spectrometer);
	EXPORTED void STDCALL Spectrometer_setSpectrumReadThrottleMilliseconds_stdcall(SPECTROMETER_T c_spectrometer, int value);
	EXPORTED int STDCALL Spectrometer_getSocketTimeoutMilliseconds_stdcall(SPECTROMETER_T c_spectrometer);
	EXPORTED void STDCALL Spectrometer_setSocketTimeoutMilliseconds_stdcall(SPECTROMETER_T c_spectrometer, int value);
	EXPORTED short STDCALL Spectrometer_isCheckForBytesAvailableEnabled_stdcall(SPECTROMETER_T c_spectrometer);
	EXPORTED void STDCALL Spectrometer_setCheckForBytesAvailable_stdcall(SPECTROMETER_T c_spectrometer, short value);
	EXPORTED int STDCALL Spectrometer_getReadSpectrumRetryLimit_stdcall(SPECTROMETER_T c_spectrometer);
	EXPORTED void STDCALL Spectrometer_setReadSpectrumRetryLimit_stdcall(SPECTROMETER_T c_spectrometer, int value);
	EXPORTED short STDCALL Spectrometer_allowWriteToEEPROM_stdcall(SPECTROMETER_T c_spectrometer, int privilegeLevel, int slot);
	EXPORTED void STDCALL Spectrometer_getSpectrumBase_stdcall(SPECTROMETER_T c_spectrometer, SPECTROMETERINFO_T retval);
	EXPORTED void STDCALL Spectrometer_setStabilityScan_stdcall(SPECTROMETER_T c_spectrometer, short on);
	EXPORTED short STDCALL Spectrometer_isStabilityScan_stdcall(SPECTROMETER_T c_spectrometer);
	EXPORTED int STDCALL Spectrometer_getIntegrationTimeMinimum_stdcall(SPECTROMETER_T c_spectrometer);
	EXPORTED int STDCALL Spectrometer_getIntegrationTimeMaximum_stdcall(SPECTROMETER_T c_spectrometer);
	EXPORTED int STDCALL Spectrometer_getIntegrationTimeIncrement_stdcall(SPECTROMETER_T c_spectrometer);
	EXPORTED int STDCALL Spectrometer_getIntegrationTimeBase_stdcall(SPECTROMETER_T c_spectrometer);
	EXPORTED int STDCALL Spectrometer_getActualIntegrationTime_stdcall(SPECTROMETER_T c_spectrometer);
	EXPORTED void STDCALL Spectrometer_getChannels_stdcall(SPECTROMETER_T c_spectrometer, SPECTROMETERCHANNELARRAY_T retval);
	EXPORTED int STDCALL Spectrometer_getNumberOfChannels_stdcall(SPECTROMETER_T c_spectrometer);
	EXPORTED int STDCALL Spectrometer_getNumberOfEnabledChannels_stdcall(SPECTROMETER_T c_spectrometer);
	EXPORTED void STDCALL Spectrometer_getChannelIndices_stdcall(SPECTROMETER_T c_spectrometer, INTARRAY_T retval);
	EXPORTED short STDCALL Spectrometer_isNetworkSpectrometer_stdcall(SPECTROMETER_T c_spectrometer);
	EXPORTED short STDCALL Spectrometer_isRotatorEnabled_stdcall(SPECTROMETER_T c_spectrometer);
	EXPORTED void STDCALL Spectrometer_setRotatorEnabled_stdcall(SPECTROMETER_T c_spectrometer, short rotator);
	EXPORTED int STDCALL Spectrometer_getBenchSlot_stdcall(SPECTROMETER_T c_spectrometer);
	EXPORTED int STDCALL Spectrometer_getSpectrometerConfigSlot_stdcall(SPECTROMETER_T c_spectrometer);
	EXPORTED int STDCALL Spectrometer_getDetectorSerialNumberSlot_stdcall(SPECTROMETER_T c_spectrometer);
	EXPORTED int STDCALL Spectrometer_getCPLDVersionSlot_stdcall(SPECTROMETER_T c_spectrometer);
	EXPORTED void STDCALL Spectrometer_getConfigurationFromSpectrometer_stdcall(SPECTROMETER_T c_spectrometer);
	EXPORTED void STDCALL Spectrometer_setConfiguration_stdcall(SPECTROMETER_T c_spectrometer);
	EXPORTED void STDCALL Spectrometer_getNewCoefficients_stdcall(SPECTROMETER_T c_spectrometer, int index, COEFFICIENTS_T retval);
	EXPORTED void STDCALL Spectrometer_getCoefficientsFromSpectrometer_stdcall(SPECTROMETER_T c_spectrometer);
	EXPORTED void STDCALL Spectrometer_setCoefficients_stdcall(SPECTROMETER_T c_spectrometer);
	EXPORTED int STDCALL Spectrometer_getFirmwareVersionNumber_stdcall(SPECTROMETER_T c_spectrometer);
	EXPORTED void STDCALL Spectrometer_getCodeVersion_stdcall(SPECTROMETER_T c_spectrometer, JSTRING_T fileName, JSTRING_T retval);
	EXPORTED int STDCALL Spectrometer_getMaxIntensity_stdcall(SPECTROMETER_T c_spectrometer);
	EXPORTED short STDCALL Spectrometer_isStrobeDelayEnabled_stdcall(SPECTROMETER_T c_spectrometer);
	EXPORTED void STDCALL Spectrometer_close_stdcall(SPECTROMETER_T c_spectrometer);
	EXPORTED void STDCALL Spectrometer_Destroy_stdcall(SPECTROMETER_T spectrometer);
#endif /* WIN32 */
#ifdef __cplusplus
};
#endif /* __cplusplus */
#endif /* SPECTROMETER_H */
