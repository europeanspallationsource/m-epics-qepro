/**
 * File: IndyImpl.h
 * Autogenerated on Mon Apr 11 10:06:05 EDT 2016 based on
 * IndyImpl.java
 * for the Java class
 * com/oceanoptics/omnidriver/features/indy/IndyImpl
 * Copyright (C) 2006 - 2008 Ocean Optics, Inc.  All rights reserved.
 */


#ifndef INDYIMPL_H
#define INDYIMPL_H
#include "BaseJavaClass.h"

#ifdef __cplusplus

#ifdef INCLUDES_IN_HEADER
#include "IndyCurrentSampleArray.h"
#include "JStringArray.h"
#include "IndyVoltageSampleArray.h"
#endif /* INCLUDES_IN_HEADER */

/* Pre-declarations for circular dependencies in header files */
#ifdef CLASS_PREDECLARATIONS
class IndyCurrentSampleArray;
class JStringArray;
class IndyVoltageSampleArray;
#endif /* CLASS_PREDECLARATIONS */

CLASS_TOKEN EXPORTED IndyImpl : public BaseJavaClass // CPPClass.tag001
{
public: 
	int getCurrentOutputCalibration4mA(int moduleIndex, int channel);
	int getCurrentOutputCalibration20mA(int moduleIndex, int channel);
	ShortArray getCurrentOutputEnables(int moduleIndex);
	ShortArray getCurrentOutputEnergized(int moduleIndex);
	int getCurrentOutputMaximumCounts(int moduleIndex, int channel);
	IndyCurrentSampleArray getCurrentOutputs(int moduleIndex);
	short getExcitationEnable(int moduleIndex, int channel);
	float getExcitationVoltage(int moduleIndex, int channel);
	FloatArray getExcitationVoltageOptions(int moduleIndex, int channel);
	JStringArray getFeatureGUIClassnames();
	int getIndyGPIOInputValues(int moduleIndex);
	int getIndyGPIOOutputValues(int moduleIndex);
	int getIndyGPIOOutputEnables(int moduleIndex);
	int getNumberOfCurrentInputs(int moduleIndex);
	int getNumberOfCurrentOutputs(int moduleIndex);
	int getNumberOfIndyGPIO(int moduleIndex);
	int getNumberOfIndyModules();
	int getNumberOfVoltageInputs(int moduleIndex);
	int getNumberOfVoltageOutputs(int moduleIndex);
	int getVoltageOutputMaximumCounts(int moduleIndex, int channel);
	IndyVoltageSampleArray getVoltageOutputs(int moduleIndex);
	IndyCurrentSampleArray sampleCurrentInputs(int moduleIndex);
	IndyVoltageSampleArray sampleVoltageInputs(int moduleIndex);
	void setCurrentOutputCalibration4mA(int moduleIndex, int channel, int counts);
	void setCurrentOutputCalibration20mA(int moduleIndex, int channel, int counts);
	void setCurrentOutputCounts(int moduleIndex, int channel, int counts);
	void setCurrentOutputEnable(int moduleIndex, int channel, short enabled);
	void setCurrentOutputMilliamps(int moduleIndex, int channel, float milliamps);
	void setExcitationEnable(int moduleIndex, int channel, short enabled);
	void setExcitationVoltage(int moduleIndex, int channel, float volts);
	void setIndyGPIOConfiguration(int moduleIndex, int outputValueVector, int enableVector, int mask);
	void setIndyGPIOOutputValues(int moduleIndex, int valueVector, int mask);
	void setVoltageOutputCounts(int moduleIndex, int channel, int counts);
	void setVoltageOutputVolts(int moduleIndex, int channel, float volts);
	~IndyImpl();
	// No public default Java constructor; creating one:
	IndyImpl();
	// No public Java copy constructor; creating one:
	IndyImpl(const IndyImpl &that);
	// Creating assignment operator declaration:
	IndyImpl &operator=(const IndyImpl &that);

private:
	jmethodID mid_getCurrentOutputCalibration4mA1327;
	jmethodID mid_getCurrentOutputCalibration20mA1328;
	jmethodID mid_getCurrentOutputEnables1329;
	jmethodID mid_getCurrentOutputEnergized1330;
	jmethodID mid_getCurrentOutputMaximumCounts1331;
	jmethodID mid_getCurrentOutputs1332;
	jmethodID mid_getExcitationEnable1333;
	jmethodID mid_getExcitationVoltage1334;
	jmethodID mid_getExcitationVoltageOptions1335;
	jmethodID mid_getFeatureGUIClassnames1336;
	jmethodID mid_getIndyGPIOInputValues1337;
	jmethodID mid_getIndyGPIOOutputValues1338;
	jmethodID mid_getIndyGPIOOutputEnables1339;
	jmethodID mid_getNumberOfCurrentInputs1340;
	jmethodID mid_getNumberOfCurrentOutputs1341;
	jmethodID mid_getNumberOfIndyGPIO1342;
	jmethodID mid_getNumberOfIndyModules1343;
	jmethodID mid_getNumberOfVoltageInputs1344;
	jmethodID mid_getNumberOfVoltageOutputs1345;
	jmethodID mid_getVoltageOutputMaximumCounts1346;
	jmethodID mid_getVoltageOutputs1347;
	jmethodID mid_sampleCurrentInputs1348;
	jmethodID mid_sampleVoltageInputs1349;
	jmethodID mid_setCurrentOutputCalibration4mA1350;
	jmethodID mid_setCurrentOutputCalibration20mA1351;
	jmethodID mid_setCurrentOutputCounts1352;
	jmethodID mid_setCurrentOutputEnable1353;
	jmethodID mid_setCurrentOutputMilliamps1354;
	jmethodID mid_setExcitationEnable1355;
	jmethodID mid_setExcitationVoltage1356;
	jmethodID mid_setIndyGPIOConfiguration1357;
	jmethodID mid_setIndyGPIOOutputValues1358;
	jmethodID mid_setVoltageOutputCounts1359;
	jmethodID mid_setVoltageOutputVolts1360;
	void init_ids(JNIEnv* pJNIEnv);
};

extern "C" {
#endif /* __cplusplus */
#ifndef EXTERN_TYPEDEF_INDYCURRENTSAMPLEARRAY_T
#define EXTERN_TYPEDEF_INDYCURRENTSAMPLEARRAY_T
	typedef void* INDYCURRENTSAMPLEARRAY_T;
#endif /* EXTERN_TYPEDEF_INDYCURRENTSAMPLEARRAY_T */
#ifndef EXTERN_TYPEDEF_JSTRINGARRAY_T
#define EXTERN_TYPEDEF_JSTRINGARRAY_T
	typedef void* JSTRINGARRAY_T;
#endif /* EXTERN_TYPEDEF_JSTRINGARRAY_T */
#ifndef EXTERN_TYPEDEF_INDYVOLTAGESAMPLEARRAY_T
#define EXTERN_TYPEDEF_INDYVOLTAGESAMPLEARRAY_T
	typedef void* INDYVOLTAGESAMPLEARRAY_T;
#endif /* EXTERN_TYPEDEF_INDYVOLTAGESAMPLEARRAY_T */
#ifndef EXTERN_TYPEDEF_INDYIMPL_T
#define EXTERN_TYPEDEF_INDYIMPL_T
	typedef void* INDYIMPL_T;
#endif /* EXTERN_TYPEDEF_INDYIMPL_T */

	// No public default Java constructor; creating one:
	EXPORTED INDYIMPL_T IndyImpl_Create();
	EXPORTED int IndyImpl_getCurrentOutputCalibration4mA(INDYIMPL_T c_indy_impl, int moduleIndex, int channel);
	EXPORTED int IndyImpl_getCurrentOutputCalibration20mA(INDYIMPL_T c_indy_impl, int moduleIndex, int channel);
	EXPORTED void IndyImpl_getCurrentOutputEnables(INDYIMPL_T c_indy_impl, int moduleIndex, SHORTARRAY_T retval);
	EXPORTED void IndyImpl_getCurrentOutputEnergized(INDYIMPL_T c_indy_impl, int moduleIndex, SHORTARRAY_T retval);
	EXPORTED int IndyImpl_getCurrentOutputMaximumCounts(INDYIMPL_T c_indy_impl, int moduleIndex, int channel);
	EXPORTED void IndyImpl_getCurrentOutputs(INDYIMPL_T c_indy_impl, int moduleIndex, INDYCURRENTSAMPLEARRAY_T retval);
	EXPORTED short IndyImpl_getExcitationEnable(INDYIMPL_T c_indy_impl, int moduleIndex, int channel);
	EXPORTED float IndyImpl_getExcitationVoltage(INDYIMPL_T c_indy_impl, int moduleIndex, int channel);
	EXPORTED void IndyImpl_getExcitationVoltageOptions(INDYIMPL_T c_indy_impl, int moduleIndex, int channel, FLOATARRAY_T retval);
	EXPORTED void IndyImpl_getFeatureGUIClassnames(INDYIMPL_T c_indy_impl, JSTRINGARRAY_T retval);
	EXPORTED int IndyImpl_getIndyGPIOInputValues(INDYIMPL_T c_indy_impl, int moduleIndex);
	EXPORTED int IndyImpl_getIndyGPIOOutputValues(INDYIMPL_T c_indy_impl, int moduleIndex);
	EXPORTED int IndyImpl_getIndyGPIOOutputEnables(INDYIMPL_T c_indy_impl, int moduleIndex);
	EXPORTED int IndyImpl_getNumberOfCurrentInputs(INDYIMPL_T c_indy_impl, int moduleIndex);
	EXPORTED int IndyImpl_getNumberOfCurrentOutputs(INDYIMPL_T c_indy_impl, int moduleIndex);
	EXPORTED int IndyImpl_getNumberOfIndyGPIO(INDYIMPL_T c_indy_impl, int moduleIndex);
	EXPORTED int IndyImpl_getNumberOfIndyModules(INDYIMPL_T c_indy_impl);
	EXPORTED int IndyImpl_getNumberOfVoltageInputs(INDYIMPL_T c_indy_impl, int moduleIndex);
	EXPORTED int IndyImpl_getNumberOfVoltageOutputs(INDYIMPL_T c_indy_impl, int moduleIndex);
	EXPORTED int IndyImpl_getVoltageOutputMaximumCounts(INDYIMPL_T c_indy_impl, int moduleIndex, int channel);
	EXPORTED void IndyImpl_getVoltageOutputs(INDYIMPL_T c_indy_impl, int moduleIndex, INDYVOLTAGESAMPLEARRAY_T retval);
	EXPORTED void IndyImpl_sampleCurrentInputs(INDYIMPL_T c_indy_impl, int moduleIndex, INDYCURRENTSAMPLEARRAY_T retval);
	EXPORTED void IndyImpl_sampleVoltageInputs(INDYIMPL_T c_indy_impl, int moduleIndex, INDYVOLTAGESAMPLEARRAY_T retval);
	EXPORTED void IndyImpl_setCurrentOutputCalibration4mA(INDYIMPL_T c_indy_impl, int moduleIndex, int channel, int counts);
	EXPORTED void IndyImpl_setCurrentOutputCalibration20mA(INDYIMPL_T c_indy_impl, int moduleIndex, int channel, int counts);
	EXPORTED void IndyImpl_setCurrentOutputCounts(INDYIMPL_T c_indy_impl, int moduleIndex, int channel, int counts);
	EXPORTED void IndyImpl_setCurrentOutputEnable(INDYIMPL_T c_indy_impl, int moduleIndex, int channel, short enabled);
	EXPORTED void IndyImpl_setCurrentOutputMilliamps(INDYIMPL_T c_indy_impl, int moduleIndex, int channel, float milliamps);
	EXPORTED void IndyImpl_setExcitationEnable(INDYIMPL_T c_indy_impl, int moduleIndex, int channel, short enabled);
	EXPORTED void IndyImpl_setExcitationVoltage(INDYIMPL_T c_indy_impl, int moduleIndex, int channel, float volts);
	EXPORTED void IndyImpl_setIndyGPIOConfiguration(INDYIMPL_T c_indy_impl, int moduleIndex, int outputValueVector, int enableVector, int mask);
	EXPORTED void IndyImpl_setIndyGPIOOutputValues(INDYIMPL_T c_indy_impl, int moduleIndex, int valueVector, int mask);
	EXPORTED void IndyImpl_setVoltageOutputCounts(INDYIMPL_T c_indy_impl, int moduleIndex, int channel, int counts);
	EXPORTED void IndyImpl_setVoltageOutputVolts(INDYIMPL_T c_indy_impl, int moduleIndex, int channel, float volts);
	EXPORTED void IndyImpl_Destroy(INDYIMPL_T indy_impl);

#ifdef WIN32
	// Use the following function prototypes when calling functions from Visual Basic 6
	EXPORTED int STDCALL IndyImpl_getCurrentOutputCalibration4mA_stdcall(INDYIMPL_T c_indy_impl, int moduleIndex, int channel);
	EXPORTED int STDCALL IndyImpl_getCurrentOutputCalibration20mA_stdcall(INDYIMPL_T c_indy_impl, int moduleIndex, int channel);
	EXPORTED void STDCALL IndyImpl_getCurrentOutputEnables_stdcall(INDYIMPL_T c_indy_impl, int moduleIndex, SHORTARRAY_T retval);
	EXPORTED void STDCALL IndyImpl_getCurrentOutputEnergized_stdcall(INDYIMPL_T c_indy_impl, int moduleIndex, SHORTARRAY_T retval);
	EXPORTED int STDCALL IndyImpl_getCurrentOutputMaximumCounts_stdcall(INDYIMPL_T c_indy_impl, int moduleIndex, int channel);
	EXPORTED void STDCALL IndyImpl_getCurrentOutputs_stdcall(INDYIMPL_T c_indy_impl, int moduleIndex, INDYCURRENTSAMPLEARRAY_T retval);
	EXPORTED short STDCALL IndyImpl_getExcitationEnable_stdcall(INDYIMPL_T c_indy_impl, int moduleIndex, int channel);
	EXPORTED float STDCALL IndyImpl_getExcitationVoltage_stdcall(INDYIMPL_T c_indy_impl, int moduleIndex, int channel);
	EXPORTED void STDCALL IndyImpl_getExcitationVoltageOptions_stdcall(INDYIMPL_T c_indy_impl, int moduleIndex, int channel, FLOATARRAY_T retval);
	EXPORTED void STDCALL IndyImpl_getFeatureGUIClassnames_stdcall(INDYIMPL_T c_indy_impl, JSTRINGARRAY_T retval);
	EXPORTED int STDCALL IndyImpl_getIndyGPIOInputValues_stdcall(INDYIMPL_T c_indy_impl, int moduleIndex);
	EXPORTED int STDCALL IndyImpl_getIndyGPIOOutputValues_stdcall(INDYIMPL_T c_indy_impl, int moduleIndex);
	EXPORTED int STDCALL IndyImpl_getIndyGPIOOutputEnables_stdcall(INDYIMPL_T c_indy_impl, int moduleIndex);
	EXPORTED int STDCALL IndyImpl_getNumberOfCurrentInputs_stdcall(INDYIMPL_T c_indy_impl, int moduleIndex);
	EXPORTED int STDCALL IndyImpl_getNumberOfCurrentOutputs_stdcall(INDYIMPL_T c_indy_impl, int moduleIndex);
	EXPORTED int STDCALL IndyImpl_getNumberOfIndyGPIO_stdcall(INDYIMPL_T c_indy_impl, int moduleIndex);
	EXPORTED int STDCALL IndyImpl_getNumberOfIndyModules_stdcall(INDYIMPL_T c_indy_impl);
	EXPORTED int STDCALL IndyImpl_getNumberOfVoltageInputs_stdcall(INDYIMPL_T c_indy_impl, int moduleIndex);
	EXPORTED int STDCALL IndyImpl_getNumberOfVoltageOutputs_stdcall(INDYIMPL_T c_indy_impl, int moduleIndex);
	EXPORTED int STDCALL IndyImpl_getVoltageOutputMaximumCounts_stdcall(INDYIMPL_T c_indy_impl, int moduleIndex, int channel);
	EXPORTED void STDCALL IndyImpl_getVoltageOutputs_stdcall(INDYIMPL_T c_indy_impl, int moduleIndex, INDYVOLTAGESAMPLEARRAY_T retval);
	EXPORTED void STDCALL IndyImpl_sampleCurrentInputs_stdcall(INDYIMPL_T c_indy_impl, int moduleIndex, INDYCURRENTSAMPLEARRAY_T retval);
	EXPORTED void STDCALL IndyImpl_sampleVoltageInputs_stdcall(INDYIMPL_T c_indy_impl, int moduleIndex, INDYVOLTAGESAMPLEARRAY_T retval);
	EXPORTED void STDCALL IndyImpl_setCurrentOutputCalibration4mA_stdcall(INDYIMPL_T c_indy_impl, int moduleIndex, int channel, int counts);
	EXPORTED void STDCALL IndyImpl_setCurrentOutputCalibration20mA_stdcall(INDYIMPL_T c_indy_impl, int moduleIndex, int channel, int counts);
	EXPORTED void STDCALL IndyImpl_setCurrentOutputCounts_stdcall(INDYIMPL_T c_indy_impl, int moduleIndex, int channel, int counts);
	EXPORTED void STDCALL IndyImpl_setCurrentOutputEnable_stdcall(INDYIMPL_T c_indy_impl, int moduleIndex, int channel, short enabled);
	EXPORTED void STDCALL IndyImpl_setCurrentOutputMilliamps_stdcall(INDYIMPL_T c_indy_impl, int moduleIndex, int channel, float milliamps);
	EXPORTED void STDCALL IndyImpl_setExcitationEnable_stdcall(INDYIMPL_T c_indy_impl, int moduleIndex, int channel, short enabled);
	EXPORTED void STDCALL IndyImpl_setExcitationVoltage_stdcall(INDYIMPL_T c_indy_impl, int moduleIndex, int channel, float volts);
	EXPORTED void STDCALL IndyImpl_setIndyGPIOConfiguration_stdcall(INDYIMPL_T c_indy_impl, int moduleIndex, int outputValueVector, int enableVector, int mask);
	EXPORTED void STDCALL IndyImpl_setIndyGPIOOutputValues_stdcall(INDYIMPL_T c_indy_impl, int moduleIndex, int valueVector, int mask);
	EXPORTED void STDCALL IndyImpl_setVoltageOutputCounts_stdcall(INDYIMPL_T c_indy_impl, int moduleIndex, int channel, int counts);
	EXPORTED void STDCALL IndyImpl_setVoltageOutputVolts_stdcall(INDYIMPL_T c_indy_impl, int moduleIndex, int channel, float volts);
	EXPORTED void STDCALL IndyImpl_Destroy_stdcall(INDYIMPL_T indy_impl);
#endif /* WIN32 */
#ifdef __cplusplus
};
#endif /* __cplusplus */
#endif /* INDYIMPL_H */
