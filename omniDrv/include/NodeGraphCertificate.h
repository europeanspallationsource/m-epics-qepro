/**
 * File: NodeGraphCertificate.h
 * Autogenerated on Mon Apr 11 10:06:41 EDT 2016 based on
 * NodeGraphCertificate.java
 * for the Java class
 * com/oceanoptics/spectralprocessing/NodeGraphCertificate
 * Copyright (C) 2006 - 2008 Ocean Optics, Inc.  All rights reserved.
 */


#ifndef NODEGRAPHCERTIFICATE_H
#define NODEGRAPHCERTIFICATE_H
#include "BaseJavaClass.h"

#ifdef __cplusplus

#ifdef INCLUDES_IN_HEADER
#endif /* INCLUDES_IN_HEADER */


CLASS_TOKEN EXPORTED NodeGraphCertificate : public BaseJavaClass // CPPClass.tag001
{
public: 
	NodeGraphCertificate(short isTransient);
	~NodeGraphCertificate();
	// No public default Java constructor; creating one:
	NodeGraphCertificate();
	// No public Java copy constructor; creating one:
	NodeGraphCertificate(const NodeGraphCertificate &that);
	// Creating assignment operator declaration:
	NodeGraphCertificate &operator=(const NodeGraphCertificate &that);

private:
	jmethodID mid_NodeGraphCertificate5808;
	void init_ids(JNIEnv* pJNIEnv);
};

extern "C" {
#endif /* __cplusplus */
#ifndef EXTERN_TYPEDEF_NODEGRAPHCERTIFICATE_T
#define EXTERN_TYPEDEF_NODEGRAPHCERTIFICATE_T
	typedef void* NODEGRAPHCERTIFICATE_T;
#endif /* EXTERN_TYPEDEF_NODEGRAPHCERTIFICATE_T */

	// No public default Java constructor; creating one:
	EXPORTED NODEGRAPHCERTIFICATE_T NodeGraphCertificate_Create();
	EXPORTED NODEGRAPHCERTIFICATE_T NodeGraphCertificate_Create_1(short isTransient);
	EXPORTED void NodeGraphCertificate_Destroy(NODEGRAPHCERTIFICATE_T node_graph_certificate);

#ifdef WIN32
	// Use the following function prototypes when calling functions from Visual Basic 6
	EXPORTED NODEGRAPHCERTIFICATE_T STDCALL NodeGraphCertificate_Create_stdcall_1(short isTransient);
	EXPORTED void STDCALL NodeGraphCertificate_Destroy_stdcall(NODEGRAPHCERTIFICATE_T node_graph_certificate);
#endif /* WIN32 */
#ifdef __cplusplus
};
#endif /* __cplusplus */
#endif /* NODEGRAPHCERTIFICATE_H */
