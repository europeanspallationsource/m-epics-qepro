/****************************************************************************************************************** 
 * File:
 * 	devUSBSpectrometers.cpp
 * Description:
 * 	This file contains the EPICS device support routines.
 *	It is based on the original file written by David Beauregard from Canadian Light Source.
 *	However, the original file did not fully support the OceanView QEPro spectrometer.
 *	This file is specially written for support QEPro and has few changes compare to the original David's file.
 * Date:
 *	16 Aug 2017
 * Author:
 * 	Tomasz Brys 
 * 	tomasz.brys@esss.se
 * Copyright:
 * 	European Spallation Source ERIC
 *
 ******************************************************************************************************************/

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <cantProceed.h>
#include <dbDefs.h>
#include <dbAccess.h>
#include <recSup.h>
#include <recGbl.h>
#include <devSup.h>
#include <link.h>
#include <alarm.h>
#include <epicsTypes.h>
#include <aiRecord.h>
#include <biRecord.h>
#include <menuConvert.h>
#include <epicsExport.h> 
#include <dbScan.h> 
#include <ellLib.h> 
#include <cantProceed.h> 
#include <epicsThread.h>
#include <epicsMutex.h>
#include <initHooks.h>
#include "errlog.h"

#include "USBSpectrometersInterface.h"

//printf colors
#define RED     "\x1b[31m"
#define GREEN   "\x1b[32m"
#define YELLOW  "\x1b[33m"
#define BLUE    "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN    "\x1b[36m"
#define RESET   "\x1b[0m"

#define debug 1
#define debugprint(X ...)		if(debug){ printf(GREEN"[DEBUG]:"RESET X); }

enum ioType
	{
	UNKNOWN_IOTYPE,
	GET_AXIS,
	SET_INDEX,
        SET_CHANNEL,
        SET_INTEGRATION,
        SET_AVERAGES,
        SET_BOXCAR,
        SET_ITERATIONS,
        GET_TRIGGER_MODE,
        SET_TRIGGER_MODE,
        SET_STROBE_ENABLE,
        GET_INTEGRATION,
        GET_AVERAGES,
        GET_BOXCAR,
        GET_ITERATIONS,
        GET_DETECTOR_TEMP,
        GET_BOARD_TEMP,
        GET_DETECTOR_TEMP_SP,
        SET_DETECTOR_TEMP_SP,
	GET_DARK_PIXELS,
	GET_PIXELS,
	GET_MIN_INTEGRATION,	
	GET_MAX_INTEGRATION,	
	GET_MAX_INTENSITY,	
	GET_SPECTROMETERS,
	GET_NAME,
	GET_VERSION,
	GET_MODEL,
	GET_SERIAL_NUMBER,
	GET_CONNECTION,
	GET_SPECTRUM,
	GET_CORR_ELEC_DARK,
	SET_CORR_ELEC_DARK,
	GET_PROGRESS_BAR
	};

enum ioFlag
	{
	isOutput = 0,
	isInput
	};

static struct aioType
	{
	const char *label;
	enum ioFlag flag;
	enum ioType ioType;
	double arg;
	} AioType[] =
    {
	{ "getAxis",			isInput,	GET_AXIS,		0.0 },
	{ "setIndex",			isOutput,  	SET_INDEX,		0.0 },
	{ "setChannel",        		isOutput,       SET_CHANNEL,      	0.0 },
	{ "setIntegrationTime",	   	isOutput,       SET_INTEGRATION,    	0.0 },
	{ "setAverages",       		isOutput,       SET_AVERAGES,         	0.0 },
	{ "setBoxcarWidth",         	isOutput,       SET_BOXCAR,            	0.0 },
	{ "setIterations",    		isOutput,       SET_ITERATIONS,         0.0 },
	{ "setTriggerMode",		isOutput,	SET_TRIGGER_MODE,	0.0 },
	{ "getTriggerMode",		isInput,	GET_TRIGGER_MODE,	0.0 },
	{ "setStrobe",			isOutput,	SET_STROBE_ENABLE,	0.0 },
	{ "getIntegrationTime",		isInput,	GET_INTEGRATION,	0.0 },
	{ "getAverages",		isInput,	GET_AVERAGES,		0.0 },
	{ "getBoxcarWidth",		isInput,	GET_BOXCAR,		0.0 },
	{ "getIteration",		isInput,	GET_ITERATIONS,		0.0 },
	{ "getDetectorTemperature",	isInput,	GET_DETECTOR_TEMP,	0.0 },
	{ "getBoardTemperature",	isInput,	GET_BOARD_TEMP,		0.0 },
	{ "getDetector_temp_sp",	isInput,	GET_DETECTOR_TEMP_SP,	0.0 },
	{ "setDetector_temp_sp",	isOutput,	SET_DETECTOR_TEMP_SP,	0.0 },
	{ "getNumberOfDarkPixels",	isInput,	GET_DARK_PIXELS,	0.0 },
	{ "getNumberOfPixels",		isInput,	GET_PIXELS,		0.0 },
	{ "getMinIntegration",		isInput,	GET_MIN_INTEGRATION,	0.0 },
	{ "getMaxIntegration",		isInput,	GET_MAX_INTEGRATION,	0.0 },
	{ "getMaxIntensity",		isInput,	GET_MAX_INTENSITY,	0.0 },
	{ "getSpectrometers",		isInput,	GET_SPECTROMETERS,	0.0 },
	{ "getName",			isInput,	GET_NAME,		0.0 },
	{ "getFirmwareVersion",		isInput,	GET_VERSION,		0.0 },
	{ "getFirmwareModel",		isInput,	GET_MODEL,		0.0 },
	{ "getSerialNumber",		isInput,	GET_SERIAL_NUMBER,	0.0 },
	{ "initDevice",			isInput,	GET_CONNECTION,		0.0 },
	{ "getSpectrum",		isInput,	GET_SPECTRUM,		0.0 },
	{ "getCorrectForElectricalDark",isInput,	GET_CORR_ELEC_DARK,	0.0 },
	{ "setCorrectForElectricalDark",isOutput,	SET_CORR_ELEC_DARK,	0.0 },
	{ "getProgressBar",		isInput,	GET_PROGRESS_BAR,	0.0 },
    };



#define AIO_TYPE_SIZE    (sizeof (AioType) / sizeof (struct aioType))

/*****************************************************************************************
 * Private device information
 *****************************************************************************************/
struct USBSpectrometerDev {
	int   index;			/* specrtometer index */
	enum  ioType ioType;		/* iotype needed to distinguis commands */
	};

/*****************************************************************************************
 * Some useful functions
 *****************************************************************************************/

int glb_index = 0;

static enum ioType findAioType(enum ioFlag ioFlag, char *param) {
	char *pfun = strtok(param+1," ");
	char *parg = strtok(NULL," ");
	for (unsigned int i = 0; i < AIO_TYPE_SIZE; i++){
		// param+1 to omit first char (@)
		if (strcmp(pfun, AioType[i].label) == 0  &&  AioType[i].flag == ioFlag)
			{
                        //printf("function: %s\n", pfun);
                        if(parg != NULL) printf("arg:      %s\n", parg);
			return AioType[i].ioType;
			}
        }
	return UNKNOWN_IOTYPE;
}

/************************************************************************************************
 *  devUSBSpectrometersBi
 ************************************************************************************************/

  	double stepSize = 0;
  	unsigned int integrationTime = 50000000;
  	unsigned int averages = 1;

typedef long (*DEVSUPFUN_BI)(struct biRecord *);
static  long init_record_bi (struct biRecord *pbi);
static  long read_bi (struct biRecord *pbi);
//static  long special_linconv_bi(struct biRecord *pai, int after);

static struct {
	long         number;
	DEVSUPFUN_BI report;
	DEVSUPFUN_BI init;
	DEVSUPFUN_BI init_record;
	DEVSUPFUN_BI get_ioint_info;
	DEVSUPFUN_BI read;
	DEVSUPFUN_BI special_linconv_bi;
	} devUSBSpectrometersBi =
		{
		6,
		NULL,
		NULL,
		init_record_bi,
		NULL,
		read_bi,
		NULL
		};
//extern "C"{
epicsExportAddress(dset, devUSBSpectrometersBi);
//}

static long
init_record_bi (struct biRecord *pbi) {

	struct USBSpectrometerDev *priv ;

        if (pbi->inp.type != INST_IO) {
           errlogPrintf("%s: init_record_bi INP field type should be INST_IO\n", pbi->name);
           return(S_db_badField);
        }

	priv = (USBSpectrometerDev *)malloc(sizeof(struct USBSpectrometerDev));
        if(!priv){
           recGblRecordError(S_db_noMemory, (void*)pbi, "init_record_bi failed to allocate private struct");
           return S_db_noMemory;
	}

	priv->ioType = findAioType(isInput, pbi->inp.text);
	if (priv->ioType == UNKNOWN_IOTYPE) {
            errlogPrintf(": init_record_bi Invalid type: \"@%s\"\n", pbi->name);
            return(S_db_badField);
	}


	pbi->dpvt = priv;
	return 0;
}


static long
read_bi (struct biRecord *pbi){

	int returnState = 0;

	struct USBSpectrometerDev *priv = (struct USBSpectrometerDev *)pbi->dpvt;

	switch (priv->ioType)
		{
	case GET_CONNECTION:
		pbi->val = initDevice();
	        if(pbi->val == TRUE){ 
		   priv->index = 0; // at the moment is only one spectrometer supported and index must be 0
		   printf(GREEN "[INFO] Connection to the device, index: %d\n" RESET, (int)pbi->val);
		}
                else{
		   printf(RED "[ERROR] There is a connection to the device: %d\n" RESET, (int)pbi->val);
		   priv->index = -1;
                   returnState = -1;
		}
                break;
	case GET_CORR_ELEC_DARK:
		pbi->val = getCorrectForElectricalDark(glb_index);
		break;
        default:
                returnState = -1;
		break;
	}


	if (returnState < 0)
		{
		if (recGblSetSevr(pbi, READ_ALARM, INVALID_ALARM)  &&  errVerbose
		    &&  (pbi->stat != READ_ALARM  ||  pbi->sevr != INVALID_ALARM))
			errlogPrintf("%s: Read Error\n", pbi->name);
		return 2;
		}

	pbi->dpvt = priv;
	return 0;
}


/************************************************************************************************
 *  devUSBSpectrometersBo
 ************************************************************************************************/

#include <boRecord.h>

typedef long (*DEVSUPFUN_BO)(struct boRecord *);
static  long init_record_bo (struct boRecord *pbo);
static  long write_bo (struct boRecord *pbo);

static struct {
	long         number;
	DEVSUPFUN_BO report;
	DEVSUPFUN_BO init;
	DEVSUPFUN_BO init_record;
	DEVSUPFUN_BO get_ioint_info;
	DEVSUPFUN_BO write;
	DEVSUPFUN_BO special_linconv_bo;
	} devUSBSpectrometersBo =
		{
		6,
		NULL,
		NULL,
		init_record_bo,
		NULL,
		write_bo,
		NULL
		};
//extern "C"{
epicsExportAddress(dset, devUSBSpectrometersBo);
//}

static long
init_record_bo (struct boRecord *pbo) {

	struct USBSpectrometerDev *priv ;

        if (pbo->out.type != INST_IO) {
           errlogPrintf("%s: init_record_bo INP field type should be INST_IO\n", pbo->name);
           return(S_db_badField);
        }

	priv = (USBSpectrometerDev *)malloc(sizeof(struct USBSpectrometerDev));
        if(!priv){
           recGblRecordError(S_db_noMemory, (void*)pbo, "init_record_bi failed to allocate private struct");
           return S_db_noMemory;
	}

	priv->ioType = findAioType(isOutput, pbo->out.text);
	if (priv->ioType == UNKNOWN_IOTYPE) {
            errlogPrintf(": init_record_bo Invalid type: \"@%s\"\n", pbo->name);
            return(S_db_badField);
	}


	pbo->dpvt = priv;
	return 0;
}


static long
write_bo (struct boRecord *pbo){

	int returnState = 0;
	printf("...DUPA...DUPA...DUPA...\n");

	struct USBSpectrometerDev *priv = (struct USBSpectrometerDev *)pbo->dpvt;

	switch (priv->ioType)
		{
	case SET_CORR_ELEC_DARK:
		printf("...call setCorrectForElectricalDark, index %d, val %d\n", glb_index, (int)pbo->val);
		setCorrectForElectricalDark(glb_index, (int)pbo->val);
		break;
        default:
                returnState = -1;
		break;
	}


	if (returnState < 0)
		{
		if (recGblSetSevr(pbo, READ_ALARM, INVALID_ALARM)  &&  errVerbose
		    &&  (pbo->stat != READ_ALARM  ||  pbo->sevr != INVALID_ALARM))
			errlogPrintf("%s: Read Error\n", pbo->name);
		return 2;
		}

	pbo->dpvt = priv;
	return 0;
}

/************************************************************************************************
 *  devUSBSpectrometersAi
 ************************************************************************************************/
static long init_record_ai (struct aiRecord *pai);
static long read_ai (struct aiRecord *pai);
//static long special_linconv_ai(struct aiRecord *pai, int after);

typedef long (*DEVSUPFUN_AI)(struct aiRecord *);

double  glb_progressBar = 0;

static struct
	{
	long         number;
	DEVSUPFUN_AI report;
	DEVSUPFUN_AI init;
	DEVSUPFUN_AI init_record;
	DEVSUPFUN_AI get_ioint_info;
	DEVSUPFUN_AI read;
	DEVSUPFUN_AI special_linconv;
	} devUSBSpectrometersAi =
		{
		6,
		NULL,
		NULL,
		init_record_ai,
		NULL,
		read_ai,
		NULL
		};
//extern "C"{
epicsExportAddress(dset, devUSBSpectrometersAi);
//}



static long
init_record_ai (struct aiRecord *pai)
{
	struct USBSpectrometerDev *priv;
	unsigned long index;


        if (pai->inp.type != INST_IO) {
            errlogPrintf("%s: init_record_ai INP field type should be INST_IO\n", pai->name);
            return(S_db_badField);
        }
        
	priv = (USBSpectrometerDev *)malloc(sizeof(struct USBSpectrometerDev));
        if(!priv){
           recGblRecordError(S_db_noMemory, (void*)pai, "devBiTimebase failed to allocate private struct");
           return S_db_noMemory;
	}

	priv->ioType = findAioType(isInput, pai->inp.text);
	if (priv->ioType == UNKNOWN_IOTYPE) {
            errlogPrintf(": init_record_ai Invalid type: \"@%s\"\n", pai->name);
            return(S_db_badField);
	}

	recGblInitConstantLink(&pai->inp, DBF_ULONG, &index);
	priv->index = index;
	pai->dpvt = priv;
	return 0;
}


static long
read_ai (struct aiRecord *pai){

	int returnState = 0;

	struct USBSpectrometerDev *priv = (struct USBSpectrometerDev *)pai->dpvt;

	switch (priv->ioType)
		{

	case GET_AVERAGES:
		pai->rval = getAverages(glb_index);
                break;

        case GET_BOXCAR:
		pai->rval = getBoxcarWidth(glb_index);
                break;

        case GET_INTEGRATION:
		pai->rval = getIntegrationTime(glb_index);
                break;

	case GET_DARK_PIXELS:
		pai->rval = getNumberOfDarkPixels(glb_index);
                break;

	case GET_PIXELS:
		pai->rval = getNumberOfPixels(glb_index);
                break;

	case GET_SPECTROMETERS:
		pai->rval = getSpectrometers();
                break;

	case GET_MIN_INTEGRATION:
		pai->rval = getMinIntegration(glb_index);
                break;

	case GET_MAX_INTEGRATION:
		pai->rval = getMaxIntegration(glb_index);
                break;

	case GET_MAX_INTENSITY:
		pai->rval = getMaxIntensity(glb_index);
                break;

	case GET_DETECTOR_TEMP:
		pai->val = getDetectorTemperature(glb_index);
		returnState = 2;
                break;

	case GET_TRIGGER_MODE:
		pai->rval = getTriggerMode(glb_index);
                break;

	case GET_BOARD_TEMP:
		pai->val = getBoardTemperature(glb_index);
		returnState = 2;
                break;

	case GET_PROGRESS_BAR:
               	if(stepSize == 0){
    			stepSize = integrationTime * averages / 1000000000. ;
    		}
		//printf("scan value: %d\n", pai->scan);
		glb_progressBar += stepSize;
		pai->val = glb_progressBar;
		returnState = 2;
                break;

        default:
		printf("default in read_ai: %s\n", pai->name);
                returnState = -1;
		break;
	}


	if (returnState < 0)
		{
		if (recGblSetSevr(pai, READ_ALARM, INVALID_ALARM)  &&  errVerbose
		    &&  (pai->stat != READ_ALARM  ||  pai->sevr != INVALID_ALARM))
			errlogPrintf("%s: Read Error\n", pai->name);
		return 2;
		}


	return returnState;
}

/***********************************************************************************************************
 *  devUSBSpectrometersAo
 **********************************************************************************************************/

#include <aoRecord.h>

typedef long (*DEVSUPFUN_AO)(struct aoRecord *);

static long init_record_ao(struct aoRecord *pao);
static long write_ao (struct aoRecord *pao);
//static long special_linconv(struct aoRecord *pao, int after);

static struct
	{
	long         number;
	DEVSUPFUN_AO report;
	DEVSUPFUN_AO init;
	DEVSUPFUN_AO init_record;
	DEVSUPFUN_AO get_ioint_info;
	DEVSUPFUN_AO write_lout;
	DEVSUPFUN_AO special_linconv;
	} devUSBSpectrometersAo =
		{
		6,
		NULL,
		NULL,
		init_record_ao,
		NULL,
		write_ao,
		NULL
		};

//extern "C" {
epicsExportAddress(dset, devUSBSpectrometersAo);
//}

static long
init_record_ao (struct aoRecord *pao) {

	struct USBSpectrometerDev *priv;


        if (pao->out.type != INST_IO) {
            errlogPrintf("%s: init_record_ao INP field type should be INST_IO\n", pao->name);
            return(S_db_badField);
        }

	priv = (USBSpectrometerDev *)malloc(sizeof(struct USBSpectrometerDev));
        if(!priv){
           recGblRecordError(S_db_noMemory, (void*)pao, "devBiTimebase failed to allocate private struct");
           return S_db_noMemory;
	}

	priv->ioType = findAioType(isOutput, pao->out.text);
	if (priv->ioType == UNKNOWN_IOTYPE) {
            errlogPrintf(": init_record_ao Invalid type: \"%s\"\n", pao->name);
            return(S_db_badField);
	}

//	pao->udf = FALSE;
	pao->dpvt = priv;

	return 2;
}

static long
write_ao (struct aoRecord *pao)
{

	struct USBSpectrometerDev *priv = (struct USBSpectrometerDev *)pao->dpvt;
	int returnState = 0;

	switch (priv->ioType)
                {
        case SET_INDEX:
               // glb_index = (int)pao->val;
               // debugprint("glb_index = %d\n",glb_index);
                break;
        case SET_CHANNEL:
               // glb_channel = (int)pao->val;
               // debugprint("glb_channel = %d\n",glb_channel);
                break;
        case SET_INTEGRATION:
                setIntegrationTime(glb_index, (int)pao->val);
                break;
        case SET_AVERAGES:
                setAverages(glb_index, (int)pao->val);
                break;
        case SET_BOXCAR:
                setBoxcarWidth(glb_index, (int)pao->val);
                break;
        case SET_TRIGGER_MODE:
	        printf("Call setTriggerMode, index: %d, pao->val=%d\n", glb_index, (int)pao->val);
        	setTriggerMode(glb_index, (int)pao->val);
        	break;

        case SET_DETECTOR_TEMP_SP:
        	//setDetectorTemperatureSetpoint(glb_index, pao->val);
		break;

        default:
		printf("default in write_ao: %s\n", pao->name);
                returnState = -1;
                }



	if (returnState < 0)
                {
                if (recGblSetSevr(pao, READ_ALARM, INVALID_ALARM)  &&  errVerbose
                    &&  (pao->stat != READ_ALARM  ||  pao->sevr != INVALID_ALARM))
                        errlogPrintf("%s: Read Error\n", pao->name);
                return 2;
                }


	return 0;
}

/****************************************************************************************
 * Stringin - read string 
 ****************************************************************************************/
#include <stringinRecord.h>

typedef long (*DEVSUPFUN_STRINGIN)(struct stringinRecord *);

static long init_stringin(struct stringinRecord *);
static long read_stringin(struct stringinRecord *);

static struct
        {
        long         number;
        DEVSUPFUN_STRINGIN report;
        DEVSUPFUN_STRINGIN init;
        DEVSUPFUN_STRINGIN init_record;
        DEVSUPFUN_STRINGIN get_ioint_info;
        DEVSUPFUN_STRINGIN write_lout;
        DEVSUPFUN_STRINGIN special_linconv;
        } devUSBSpectrometersStringin =
                {
                6,
                NULL,
                NULL,
                init_stringin,
                NULL,
                read_stringin,
		NULL
                };
//extern "C" {
epicsExportAddress(dset, devUSBSpectrometersStringin);
//}

static long init_stringin(struct stringinRecord * pstringin)
{
        struct USBSpectrometerDev *priv;

        if (pstringin->inp.type != INST_IO) {
            errlogPrintf("%s: init_stringin INP field type should be INST_IO\n", pstringin->name);
            return(S_db_badField);
        }

	priv = (USBSpectrometerDev *)malloc(sizeof(struct USBSpectrometerDev));
        if(!priv){
           recGblRecordError(S_db_noMemory, (void*)pstringin, "devBiTimebase failed to allocate private struct");
           return S_db_noMemory;
	}

        priv->ioType = findAioType(isInput, pstringin->inp.text);
        if (priv->ioType == UNKNOWN_IOTYPE) {
            errlogPrintf(": init_stringin Invalid type: \"@%s\"\n", pstringin->name);
            return(S_db_badField);
        }

	pstringin->dpvt = priv;
        return 0;

}

static long
read_stringin (struct stringinRecord *pstringin)
{
        int returnState;

	char name[100];
        struct USBSpectrometerDev *priv = (struct USBSpectrometerDev *)pstringin->dpvt;

        switch (priv->ioType)
                {
	case GET_NAME:
                strcpy(name, getName(glb_index));
                strcpy(pstringin->val, name); 
		returnState = 0;
		break;
	case GET_MODEL:
                strcpy(name, getFirmwareModel(glb_index));
                strcpy(pstringin->val, name); 
		returnState = 0;
		break;
	case GET_VERSION:
                strcpy(name, getFirmwareVersion(glb_index));
                strcpy(pstringin->val, name); 
		returnState = 0;
		break;
	case GET_SERIAL_NUMBER:
                strcpy(name, getSerialNumber(glb_index));
                strcpy(pstringin->val, name); 
		returnState = 0;
		break;
        default:
		debugprint("default in read_stringin: %s\n", pstringin->val);
                returnState = -1;
                }

        if (returnState < 0)
                {
                if (recGblSetSevr(pstringin, READ_ALARM, INVALID_ALARM)  &&  errVerbose
                    &&  (pstringin->stat != READ_ALARM  ||  pstringin->sevr != INVALID_ALARM))
                        errlogPrintf("%s: Read Error\n", pstringin->name);
                return 2;
                }

        //strcpy(pstringin->val, name); 
        return returnState;
}




/****************************************************************************************
 * Aai - read a data array of values
 ****************************************************************************************/

#include <aaiRecord.h>

static ELLLIST aList = ELLLIST_INIT;

struct USBSpectrometerState {
	ELLNODE 	node;
	epicsMutexId 	lock;
	IOSCANPVT	scan;
	epicsThreadId	readSpectrumThread;
	double		*spectrum;
	int		retval;
	int		progress;
	int		index;			/* specrtometer index */
	enum  ioType 	ioType;			/* iotype needed to distinguis commands */
	};


typedef long (*DEVSUPFUN_aai)(struct aaiRecord *);

static long init_aai(int);
static long init_record_aai(struct aaiRecord *);
static long get_ioint_info_aai(int dir, dbCommon *prec, IOSCANPVT* io);
static long read_aai(struct aaiRecord *);

static void worker(void* raw);
static void start_workers(initHookState state);


static struct
        {
        long         number;
        DEVSUPFUN_aai report;
        DEVSUPFUN_aai init;
        DEVSUPFUN_aai init_record;
        DEVSUPFUN_aai get_ioint_info;
        DEVSUPFUN_aai read;
        DEVSUPFUN_aai special_linconv;
        } devUSBSpectrometersAai =
                {
                6,
                NULL,
                (DEVSUPFUN_aai)init_aai,
                init_record_aai,
                (DEVSUPFUN_aai)get_ioint_info_aai,
                read_aai,
		NULL
                };
//extern "C" {
epicsExportAddress(dset, devUSBSpectrometersAai);
//}

//--------------------------------------------------------------------------------------
static long init_aai(int phase) {

  if(phase==0)
    initHookRegister( &start_workers );

  return 0;
}
//--------------------------------------------------------------------------------------
static long init_record_aai(struct aaiRecord * paai)
{
        struct USBSpectrometerState *priv;

        if (paai->inp.type != INST_IO) {
            errlogPrintf("%s: init_record_aai INP field type should be INST_IO\n", paai->name);
            return(S_db_badField);
        }

	priv = (USBSpectrometerState *)malloc(sizeof(struct USBSpectrometerState));
        if(!priv){
           recGblRecordError(S_db_noMemory, (void*)paai, "devBiTimebase failed to allocate private struct");
           return S_db_noMemory;
	}

        priv->ioType = findAioType(isInput, paai->inp.text);
        if (priv->ioType == UNKNOWN_IOTYPE) {
            errlogPrintf(": init_record_aai Invalid type: \"@%s\"\n", paai->name);
            return(S_db_badField);
        }
	else if(priv->ioType == GET_SPECTRUM){
	    printf("Initialization GetSpectrum\n");
	    scanIoInit(&priv->scan); 
	    priv->spectrum = (double*)calloc(1,sizeof(double)*paai->nelm);
  	    priv->lock = epicsMutexMustCreate(); 
	    //memset(paai->bptr, 0, paai->nelm);
  	    priv->readSpectrumThread = NULL; 
  	    ellAdd( &aList, &priv->node); 
	    paai->nord = paai->nelm;	
	    paai->udf = FALSE;	
	}
	else if(priv->ioType == GET_AXIS){
	    printf("Initialization GetXaxis\n");
	
	}
	paai->dpvt = priv;
        return 0;
}
//--------------------------------------------------------------------------------------
static long get_ioint_info_aai(int dir, dbCommon* prec, IOSCANPVT* io) {

  struct USBSpectrometerState* priv = (USBSpectrometerState*)prec->dpvt;

  *io = priv->scan;
  return 0;
}
//--------------------------------------------------------------------------------------
	

static long
read_aai (struct aaiRecord *paai)
{
        struct USBSpectrometerState *priv = (struct USBSpectrometerState *)paai->dpvt;
	int retval = 0;

        switch (priv->ioType)
                {
	case GET_AXIS:
 		retval = getAxis(glb_index, (double*)paai->bptr);
		if(retval > 0){
			paai->udf = FALSE;
			paai->nelm = retval;
			paai->nord = retval;
		}
		else
			paai->udf = TRUE;
		break;
	case GET_SPECTRUM:
		epicsMutexMustLock(priv->lock);
		memcpy(paai->bptr, priv->spectrum, priv->retval*sizeof(double));
		epicsMutexUnlock(priv->lock);

		printf(".......GetSpectrum retval = %d......\n", priv->retval);
                if     (priv->retval == -1) paai->udf = TRUE;
                else if(priv->retval == -2) paai->udf = TRUE;
                else if(priv->retval == -3) paai->udf = TRUE;
                else   { 
		   //paai->udf = FALSE;
                   paai->nelm = priv->retval;
                   paai->nord = priv->retval;
                   }

		glb_progressBar = 0;

		break;
        default:
		printf("Function not recognized, name: %s, type:%d\n", paai->name, priv->ioType);
                }

        return 0;
}
//------------------------------------------------------------------------------------------------------
static void worker(void* raw) {

  struct USBSpectrometerState* priv = (USBSpectrometerState*)raw;
  while(1) {

    epicsMutexMustLock(priv->lock);
    priv->retval = getSpectrum(glb_index, (double*)priv->spectrum);
    epicsMutexUnlock(priv->lock);

    scanIoRequest(priv->scan);

    epicsThreadSleep(1.0);
  }

}
//------------------------------------------------------------------------------------------------------
static void start_workers( initHookState state ) {

  ELLNODE *cur;
  if(state != initHookAfterInterruptAccept)
    return;

  for(cur = ellFirst( &aList ); cur; cur = ellNext(cur) ) {
      struct USBSpectrometerState *priv = CONTAINER(cur, struct USBSpectrometerState, node);
      priv->readSpectrumThread = epicsThreadMustCreate("getSpectrumThread", 
				epicsThreadPriorityMedium, 
				epicsThreadGetStackSize(epicsThreadStackSmall), 
				&worker, 
				priv);
  }

}



