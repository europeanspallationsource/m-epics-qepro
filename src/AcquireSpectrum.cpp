/****************************************************************************************************************** 
* File:
*      devUSBSpectrometers.cpp
* Description:
*      This file contains the C functions used to access the java wrapper functions
*      It is based on the original file written by David Beauregard from Canadian Light Source.
*      However, the original file did not fully support the OceanView QEPro spectrometer.
*      This file is specially written for support QEPro and has few changes compare to the original David's file.
* Date:
*      16 Aug 2017
* Author:
*      Tomasz Brys 
*      tomasz.brys@esss.se
* Copyright:
*      European Spallation Source ERIC
*
******************************************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <epicsTypes.h>

#include <iostream>
#include <fstream>

/* Includes for the OmniDriver classes.  The documentation for these
 * classes is provided separately as HTML that was derived from the
 * underlying Java code.  Note that not all methods in the Java base
 * are represented in these files, as some do not translate well to C++.
 */
#include "SpectrometerFactory.h"
#include "USBSpectrometer.h"
#include "SpectralProcessor.h"
#include "Wrapper.h"


// printf colors, not sure if it works on all terminals....
#define RED     "\x1b[31m"
#define GREEN   "\x1b[32m"
#define YELLOW  "\x1b[33m"
#define BLUE    "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN    "\x1b[36m"
#define RESET   "\x1b[0m"

// debug printf
#define debug 1
#define debugprint(X ...)               if(debug){ printf(GREEN"[DEBUG]:"RESET X); }


Wrapper wrapper;
ThermoElectricWrapper thermoElectric;

int g_NumSpec = 0;
static char name[256];


bool initDevice() {
	int numSpecs, status;
	struct stat buff;
	wrapper = Wrapper();
	int errhp;


	/* check that USB hotplug has configured the system */
	errhp = 0;
	status = -1;
	status = lstat( "/dev/oceanoptics", &buff);
	/* /dev/oceanoptics may also be a real file (shouldn't be) */
	if( status < 0){
		status = -1;
		status = stat( "/dev/oceanoptics", &buff);
	}
	/* if /dev/oceanoptics doesn't exist, fall back on old configs */
	if( status < 0){
		/* if using linux-hotplug, check to see if both required
		   hotplug files exist.  Note, existence of /etc/hotplug
		   may not preclude usage of new udev. */
		status = -1;
		status = stat("/etc/hotplug", &buff);
		if(status >= 0){
			status = -1;
			status = stat("/etc/hotplug/usb/oceanoptics", &buff);
			if(status < 0){ errhp |= 1; }
			status = -1;
			status = stat("/etc/hotplug/usb/oceanoptics.usermap", &buff);
			if(status < 0){ errhp |= 1; }
		}
		else{ errhp |= 2;}	/* apparently, a udev system */

		/* udev files exist regardless of Linux-hotplug, so we
		   have to check for new udev rules file in any case. */
		status = -1;
		status = stat("/etc/udev/rules.d/90-oceanoptics.rules", &buff);
		if(status < 0){ errhp |= 4; }

		if( errhp & 2){
			/* can only be new udev, must have rules file */
			if( errhp & 4){
				printf("ERROR: Need to install USB udev rules file\n");
				exit(1);
			}
		}
		else{
			/* if no files at all */
			if( ( errhp & 1) && ( errhp & 4)){
				printf("ERROR: Need to install either Linux-hotplug or udev USB files.\n");
				exit(1);
			}
			else{
				printf("ERROR: Probably wrong type of hotplug files installed\n");
				printf("      (Linux-hotplug versus new udev)\n");
				exit(1);
			}
		}

		//exit(1);
	}




	numSpecs = 0;
	printf("Opening all spectrometers\n");
	//int getNumberOfSpectrometersFound();
	wrapper.closeAllSpectrometers();
	g_NumSpec = numSpecs = wrapper.openAllSpectrometers();
        if(numSpecs == -1){
	  JString Jname =  wrapper.getLastException();
	  printf(RED"[ERROR]" RESET ": %s\n", Jname.getASCII());
	}
	printf("Opened %d spectrometers\n", numSpecs);

return numSpecs;
}


//------------------------------------------------------------------
char *getName(int index){

 debugprint("getName\n");
 if(!g_NumSpec) {
    strcpy(name,"No Spectrometer");
    return name;
 }
  
 JString Jname = wrapper.getName(index);
 strcpy(name, Jname.getASCII());

return name;
}
//------------------------------------------------------------------
char* getFirmwareVersion(int index){
	
 debugprint("getFirmwareVersion\n");
 if(!g_NumSpec) {
    strcpy(name,"No Spectrometer");
    return name;
 }
  
 JString Jname = wrapper.getFirmwareVersion(index);
 strcpy(name, Jname.getASCII());
 
return name;
}
//------------------------------------------------------------------
char* getFirmwareModel(int index){

 debugprint("getFirmwareModel\n");
 if(!g_NumSpec) {
    strcpy(name,"No Spectrometer");
    return name;
 }
  
 JString Jname = wrapper.getFirmwareModel(index);
 strcpy(name, Jname.getASCII());
 
return name;
}
//------------------------------------------------------------------
char* getSerialNumber(int index){

 debugprint("getSerialNumber\n");
 if(!g_NumSpec) {
    strcpy(name,"No Spectrometer");
    return name;
 }
  
 JString Jname = wrapper.getSerialNumber(index);
 strcpy(name, Jname.getASCII());
  
return name;
}
//------------------------------------------------------------------
int getNumberOfPixels(int index){
	
 debugprint("getNumberOfPixels\n");
   if(!g_NumSpec) return -1;

return wrapper.getNumberOfPixels(index);
}
//------------------------------------------------------------------
int getNumberOfDarkPixels(int index){

 debugprint("getNumberOfDarkPixels\n");
   if(!g_NumSpec) return -1;

return wrapper.getNumberOfDarkPixels(index);
}
//------------------------------------------------------------------
int getNumberOfSpectrometers(int index){

 debugprint("getNumberOfSpectrometers\n");
   if(!g_NumSpec) return -1;

return wrapper.openAllSpectrometers();
}
//------------------------------------------------------------------
void setIntegrationTime(int index, int time){

 debugprint("setIntegrationTime\n");
   if(!g_NumSpec) return;

   wrapper.setIntegrationTime(index, time);
}
//------------------------------------------------------------------
void setCorrectForElectricalDark(int index, int enable){
 debugprint("setCorrectForElectricalDark\n");
   if(!g_NumSpec) return;

   wrapper.setCorrectForElectricalDark(index, enable);
}
//------------------------------------------------------------------
int  getCorrectForElectricalDark(int index){
 debugprint("getCorrectForElectricalDark\n");
   if(!g_NumSpec) return -1;

return wrapper.getCorrectForElectricalDark(index);
}

//------------------------------------------------------------------
int getIntegrationTime(int index){
/* For some reason the integration time in the spectrometer is smaller than the minimum integration time allowed.
 * To avoid such a situation the integration time is compared to minimum and if is smaller is set to minimum.
 * On the db level apriopate fields are set to forbid operator write a wrong value.
 */
	debugprint("getIntegrationTime\n");
	if(!g_NumSpec) return -1;

	int a = wrapper.getIntegrationTime(index);
	int b = wrapper.getMinimumIntegrationTime(index);

        if(a < b){
	   wrapper.setIntegrationTime(index, b);
	   printf(YELLOW"[WARNING]" RESET ": IntegrationTime is (%d) smaller than MinimumIntegrationtime (%d)\n", a, b);
	   printf(YELLOW"[WARNING]" RESET ": Set IntegrationTime to MinimumIntegrationtime: %d ms\n", b);
	   a = wrapper.getIntegrationTime(index);
	}
	
	return a/1000;
}
//------------------------------------------------------------------
int getTriggerMode(int index){
	debugprint("getTriggerMode\n");
	if(!g_NumSpec) return -1;

	return wrapper.getExternalTriggerMode(index);
}
//------------------------------------------------------------------
void setTriggerMode(int index, int mode){
	debugprint("setTriggerMode\n");
	if(!g_NumSpec) return;

	wrapper.setExternalTriggerMode(index, mode);
}
//------------------------------------------------------------------
void setBoxcarWidth(int index, int boxcar){
	debugprint("setBoxcarWidth\n");
	
	if(!g_NumSpec) return;
	wrapper.setBoxcarWidth(index, boxcar);
}
//------------------------------------------------------------------
int     getBoxcarWidth(int index){
	debugprint("getBoxcarWidth\n");
	if(!g_NumSpec) return -1;
	
return wrapper.getBoxcarWidth(index);
}
//------------------------------------------------------------------
void setAverages(int index, int averages){
	debugprint("setAverages\n");
	if(!g_NumSpec) return;

	wrapper.setScansToAverage(index,averages);
}
//------------------------------------------------------------------
int getAverages(int index){
	debugprint("getAverages\n");
	if(!g_NumSpec) return -1;

return wrapper.getScansToAverage(index);
}
//------------------------------------------------------------------
double getBoardTemperature(int index){
	debugprint("getBoardTemperature\n");
	if(!g_NumSpec) return -999;
	double temp = 0;
	if (wrapper.isFeatureSupportedBoardTemperature(index)) {
	    BoardTemperature boardTemperature;
            boardTemperature = wrapper.getFeatureControllerBoardTemperature(index); 
            temp = boardTemperature.getBoardTemperatureCelsius();
        }
return temp;
}
//------------------------------------------------------------------
double getDetectorTemperature(int index){
	debugprint("getDetectorTemperature\n");
	if(!g_NumSpec) return -999;
	double temp = -999;
	if(wrapper.isFeatureSupportedThermoElectric(index)){
		thermoElectric = wrapper.getFeatureControllerThermoElectric(index);
		temp = thermoElectric.getDetectorTemperatureCelsius();
	}
	else{
		printf("ThermoElectric is not supported :(\n");
	}

return temp;
}
//------------------------------------------------------------------
double getDetectorTemperatureSetpoint(int index){
	debugprint("getDetectorTemperatureSetpoint\n");
	if(!g_NumSpec) return -999;
	double temp = -999;
	if(wrapper.isFeatureSupportedThermoElectric(index)){
		thermoElectric = wrapper.getFeatureControllerThermoElectric(index);
		temp = thermoElectric.getDetectorTemperatureSetPointCelsius();
	}
return temp;
}

//------------------------------------------------------------------
void setDetectorTemperatureSetpoint(int index, double temp){
	debugprint("setDetectorTemperatureSetpoint\n");
	if(!g_NumSpec) return;
	if(wrapper.isFeatureSupportedThermoElectric(index)){
		thermoElectric.setTECEnable(true);
		thermoElectric.setDetectorSetPointCelsius(temp);
	}
return;
}
//------------------------------------------------------------------
int getMinIntegration(int index){
	debugprint("getMinIntegration\n");
	if(!g_NumSpec) return -1;

return wrapper.getMinimumIntegrationTime(index);
}
//------------------------------------------------------------------
int getMaxIntegration(int index){
	debugprint("getMaxIntegration\n");
	if(!g_NumSpec) return -1;

return wrapper.getMaximumIntegrationTime(index);
}
//------------------------------------------------------------------
int getMaxIntensity(int index){
	debugprint("getMaxIntensity\n");
	if(!g_NumSpec) return -1;

return wrapper.getMaximumIntensity(index);
}
//------------------------------------------------------------------
int getSpectrometers(){
	debugprint("getSpectrometers\n");
	if(!g_NumSpec) return -1;

return wrapper.openAllSpectrometers();
}
//------------------------------------------------------------------
int getAxis(int index,  double *xaxis) {
	debugprint("getAxis %d\n", index);

	if(!g_NumSpec) return -1;

	//int numberOfPixels = wrapper.getNumberOfPixels(index);
	DoubleArray arrayX = wrapper.getWavelengths(index);
	double *wavelengths = arrayX.getDoubleValues();
	int arrayXElements = arrayX.getLength();
        

	for(int j = 0; j < arrayXElements; j++) {
         	xaxis[j] = wavelengths[j];
	}
return arrayXElements;
}
//------------------------------------------------------------------
int getSpectrum(int index, double *spectrum){
	debugprint("getSpectrum %d\n", index);

	if(!g_NumSpec) return -1;
	
	// wrapper.getSpectrum must be called first
 	DoubleArray da = wrapper.getSpectrum(index);

        if( wrapper.isSpectrumValid(index) == FALSE){
	    return -2;
	}
        if( wrapper.isSaturated(index) == TRUE){
     	   return -3;
        }

	double *values = da.getDoubleValues();
	int arrayXElements = da.getLength();

	for(int j = 0; j < arrayXElements; j++) {
		spectrum[j] = values[j];
	}
	
	static int nr = 0;
	char name[32];
	sprintf(name,"RamanSpectra%04d",nr);
	std::ofstream myfile;
 	myfile.open (name);

	for(int j = 0; j < arrayXElements; j++) {
  	    myfile << values[j] << std::endl;
	}
  	myfile.close();
        nr++;

return arrayXElements;
}

/****************************************************************************************************************** 
 ******************************************************************************************************************
 *****************************************************************************************************************/


