
#include <epicsEvent.h>
#include "dbAccess.h"

#include <epicsVersion.h>
#include <epicsTypes.h>

#include <epicsExport.h>


char*	getName(int);
char*	getFirmwareVersion(int);
char*	getFirmwareModel(int);
char*	getSerialNumber(int);
int	getNumberOfPixels(int);
int	getNumberOfDarkPixels(int);

int 	getSpectrometers();
int	getMinIntegration(int);
int	getMaxIntegration(int);
int	getMaxIntensity(int);

void	setIndex(int index);
void	setChannel(int index);

void	setIntegrationTime(int index, int time);
int	getIntegrationTime(int index);

void	setAverages(int index, int average);
int	getAverages(int index);

void 	setBoxcarWidth(int index, int boxcar);
int 	getBoxcarWidth(int index);


void 	setTriggerMode(int index, int boxcar);
int 	getTriggerMode(int index);


int	getAxis(int index, double *xaxis);
bool	initDevice();
//void	doAcquisition(int spectrometerIndex, int channel, int integration_usec, int averages, int boxcar, int iterations, int numElements, double *spectra);
void	setTriggerMode(int index, int mode);
//void	setBoxCar(int index, int boxcar);
//void	setAverages(int index, int averages);
double	getDetectorTemperature(int index);
double	getBoardTemperature(int index);
double	getDetectorTemperatureSetpoint(int index);
void	setDetectorTemperatureSetpoint(int index, double temp);
int     getSpectrum(int index, double *spectrum);
int	getCorrectForElectricalDark(int index);
void	setCorrectForElectricalDark(int index, int enable);
